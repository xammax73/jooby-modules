# jooby-modules

Welcome to jooby-modules!!

## running

    ./gradlew joobyRun

## building

    ./gradlew build

## docker

     docker build . -t jooby-modules
     docker run -p 8080:8080 -it jooby-modules
