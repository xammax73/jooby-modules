package io.xammax.jooby.hibernate.search;

import io.xammax.jooby.util.StringUtils;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.Serializable;

public class Sort implements Serializable {

  private static final long serialVersionUID = 1L;

  private static final Boolean DEFAULT_CUSTOM_EXPRESSION = false;
  private static final Direction DEFAULT_DIRECTION = Direction.ASC;
  private static final Boolean DEFAULT_IGNORE_CASE = false;

  private Boolean customExpression;
  private String property;
  private Direction direction;
  private Boolean ignoreCase;

  /**
   * Creates a new {@link Sort} instance.
   */
  public Sort() { }

  /**
   * Creates a new {@link Sort} instance.
   *
   * @param property must not be {@literal null} or {@literal empty}
   */
  public Sort(String property) {
    this(null, property, null, null);
  }

  /**
   * Creates a new {@link Sort} instance.
   *
   * @param customExpression {@literal true} if need custom query, {@literal false} otherwise
   * @param property must not be {@literal null} or {@literal empty}
   */
  public Sort(@Nullable Boolean customExpression, String property) {
    this(customExpression, property, null, null);
  }

  /**
   * Creates a new {@link Sort} instance.
   *
   * @param property must not be {@literal null} or {@literal empty}
   * @param direction {@literal ASC} if ascending sort, {@literal DESC} if descending sort
   */
  public Sort(String property, @Nullable Direction direction) {
    this(null, property, direction, null);
  }

  /**
   * Creates a new {@link Sort} instance.
   *
   * @param property must not be {@literal null} or {@literal empty}
   * @param ignoreCase {@literal true} if need to ignore case, {@literal false} otherwise
   */
  public Sort(String property, @Nullable Boolean ignoreCase) {
    this(null, property, null, ignoreCase);
  }

  /**
   * Creates a new {@link Sort} instance.
   *
   * @param customExpression {@literal true} if need custom query, {@literal false} otherwise
   * @param property must not be {@literal null} or {@literal empty}
   * @param direction {@literal ASC} if ascending sort, {@literal DESC} if descending sort
   */
  public Sort(@Nullable Boolean customExpression, String property, @Nullable Direction direction) {
    this(customExpression, property, direction, null);
  }

  /**
   * Creates a new {@link Sort} instance.
   *
   * @param property must not be {@literal null} or {@literal empty}
   * @param direction {@literal ASC} if ascending sort, {@literal DESC} if descending sort
   * @param ignoreCase {@literal true} if need to ignore case, {@literal false} otherwise
   */
  public Sort(String property, @Nullable Direction direction, @Nullable Boolean ignoreCase) {
    this(null, property, direction, ignoreCase);
  }

  /**
   * Creates a new {@link Sort} instance. If {@literal customExpression} is {@literal true},
   * need reference properties by wrapping them with curly braces ({}). For examples:
   *
   * <p><pre class="code">
   *     new Sort(true, "cast({employeeNumber} as integer)");
   *     new Sort(true, "abs({prop1} - {prop2})");
   * </pre>
   *
   * @param customExpression if {@literal null} will set {@link Sort#DEFAULT_CUSTOM_EXPRESSION}
   *                         {@literal true} if need custom query, {@literal false} otherwise
   * @param property must not be {@literal null} or {@literal empty}
   * @param direction if {@literal null} will set to default {@link Sort#DEFAULT_DIRECTION}
   * @param ignoreCase if {@literal true} sorting should be case insensitive,
   *                   if {@literal false} sorting should be case sensitive,
   *                   if {@literal null} will set to default {@link Sort#DEFAULT_IGNORE_CASE}
   */
  public Sort(@Nullable Boolean customExpression, String property, @Nullable Direction direction, @Nullable Boolean ignoreCase) {

    if (!StringUtils.hasText(property)) {
      throw new IllegalArgumentException("Property must not be null or empty!");
    }

    this.customExpression = customExpression == null ? DEFAULT_CUSTOM_EXPRESSION: customExpression;
    this.property = property;
    this.direction = direction == null ? DEFAULT_DIRECTION : direction;
    this.ignoreCase = ignoreCase == null ? DEFAULT_IGNORE_CASE : ignoreCase;
  }

  /**
   * Creates a new {@link Sort} instance. Direction defaults {@link Sort#DEFAULT_DIRECTION}.
   *
   * @param property must not be {@literal null} or {@literal empty}
   */
  public static @Nonnull Sort by(String property) {
    return new Sort(property);
  }

  /**
   * Creates a new {@link Sort} instance. Direction is {@link Direction#ASC}.
   *
   * @param property must not be {@literal null} or empty
   */
  public static @Nonnull Sort asc(String property) {
    return new Sort(property, Direction.ASC);
  }

  /**
   * Creates a new {@link Sort} instance. Direction is {@link Direction#ASC}.
   *
   * @param property must not be {@literal null} or {@literal empty}
   * @param ignoreCase must be {@literal true}, {@literal false} or {@literal null}
   */
  public static @Nonnull Sort asc(String property, Boolean ignoreCase) {
    return new Sort(property, Direction.ASC, ignoreCase);
  }

  /**
   * Creates a new {@link Sort} instance. Direction is {@link Direction#DESC}.
   *
   * @param property must not be {@literal null} or {@literal empty}
   */
  public static @Nonnull Sort desc(String property) {
    return new Sort(property, Direction.DESC);
  }

  /**
   * Creates a new {@link Sort} instance. Direction is {@link Direction#DESC}.
   *
   * @param property must not be {@literal null} or {@literal empty}
   * @param ignoreCase must be {@literal true}, {@literal false} or {@literal null}
   */
  public static @Nonnull Sort desc(String property, Boolean ignoreCase) {
    return new Sort(property, Direction.DESC, ignoreCase);
  }

  /**
   * Creates a new {@link Sort} instance. Instead of a property for this {@link Sort}, use a
   * free-form JPQL/HQL order-by expression. Reference properties by wrapping them with curly
   * braces ({}). Here are some examples:
   *
   * <p><pre>
   *     Sort.customExpressionAsc("cast({employeeno} as integer)");
   *     Sort.customExpressionAsc("abs({prop1} - {prop2})");
   * </pre>
   */
  public static @Nonnull Sort customExpressionAsc(String expression) {
    return new Sort(true, expression, Direction.ASC);
  }

  /**
   * Creates a new {@link Sort} instance. Instead of a property for this {@link Sort}, use a
   * free-form JPQL/HQL order-by expression. Reference properties by wrapping them with curly
   * braces ({}). Here are some examples:
   *
   * <p><pre>
   *     Sort.customExpressionDesc("cast({employeeno} as integer)");
   *     Sort.customExpressionDesc("abs({prop1} - {prop2})");
   * </pre>
   */
  public static @Nonnull Sort customExpressionDesc(String expression) {
    return new Sort(true, expression, Direction.DESC);
  }

  /**
   * Returns the {@literal customExpression} for order sorting. If {@literal customExpression}
   * is {@literal true}, need reference properties by wrapping them with curly braces ({}).
   * When set to {@literal true}, the {@literal ignoreCase} property is ignored. For examples:
   *
   * <p><pre class="code">
   *     new Sort(true, "cast({roleNumber} as integer)");
   *     new Sort(true, "abs({prop1} - {prop2})", Direction.fromString(String "ASC"));
   *     new Sort(true, "abs({prop1} - {prop2})", Direction.fromString(String "DESC"));
   *     Sort.customExpressionAsc("cast({roleNumber} as integer)");
   *     Sort.customExpressionDesc("abs({prop1} - {prop2})");
   * </pre>
   */
  public Boolean isCustomExpression() {
    return customExpression;
  }

  /**
   * If {@literal customExpression} is {@literal true}, need reference properties by wrapping
   * them with curly braces ({}). When set to {@literal true}, the value of {@literal ignoreCase}
   * property is ignored. For examples:
   *
   * <p><pre class="code">
   *     new Sort(true, "cast({roleNumber} as integer)");
   *     new Sort(true, "abs({prop1} - {prop2})", Direction.fromString(String "ASC"));
   *     new Sort(true, "abs({prop1} - {prop2})", Direction.fromString(String "DESC"));
   *     Sort.customExpressionAsc("cast({roleNumber} as integer)");
   *     Sort.customExpressionDesc("abs({prop1} - {prop2})");
   * </pre>
   */
  public void setCustomExpression(Boolean customExpression) {
    this.customExpression = customExpression;
  }

  /**
   * Returns the property for order sorting.
   *
   * @return the property
   */
  public String getProperty() {
    return property;
  }

  /**
   * Property by this sort.
   */
  public void setProperty(String property) {
    this.property = property;
  }

  /**
   * Returns the order the property shall be sorted to.
   *
   * @return ASC or DESC
   */
  public Direction getDirection() {
    return direction;
  }

  /**
   * Sets the direction. If {@code ASC}, sort ascending by the given {@literal property};
   * otherwise ({@literal DESC}), sort descending.
   */
  public void setDirection(Direction direction) {
    this.direction = direction;
  }

  /**
   * Returns whether sorting for this property shall be ascending.
   *
   * @return is the order ascending
   */
  public boolean isAscending() {
    return this.direction.isAscending();
  }

  /**
   * Returns whether sorting for this property shall be descending.
   *
   * @return is the order descending
   */
  public boolean isDescending() {
    return this.direction.isDescending();
  }

  /**
   * Return whether the sort ordering will be case sensitive for this property.
   *
   * @return whether or not the sort ordering will be case sensitive
   */
  public Boolean isIgnoreCase() {
    return ignoreCase;
  }

  /**
   * If {@literal true} the ordering will be case insensitive for this property. Ignore case
   * has no effect when {@literal customExpression} is specified.
   */
  public void setIgnoreCase(Boolean ignoreCase) {
    this.ignoreCase = ignoreCase;
  }

  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;

    Sort other = (Sort) obj;

    if (customExpression != other.customExpression)
      return false;
    if (direction != other.direction)
      return false;
    if (ignoreCase != other.ignoreCase)
      return false;
    if (property == null) {
      return other.property == null;
    } else return property.equals(other.property);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + (customExpression ? 1231 : 1237);
    result = prime * result + ((property == null) ? 0 : property.hashCode());
    result = prime * result + ((direction == null) ? 0 : direction.hashCode());
    result = prime * result + (ignoreCase ? 1231 : 1237);
    return result;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();

    if (customExpression) {
      sb.append("CUSTOM: ");
    }

    if (property == null) {
      sb.append("null");
    } else {
      sb.append("`");
      sb.append(property);
      sb.append("`");
    }

    sb.append(direction == Direction.DESC ? " desc" : " asc");

    if (ignoreCase && !customExpression) {
      sb.append(" (ignore case)");
    }

    return sb.toString();
  }

}