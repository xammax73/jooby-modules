package io.xammax.jooby.hibernate.search;

import io.xammax.jooby.util.TypeCheckUtils;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * A {@link Filter} is used by the {@link Filter} Search class to specify a restriction on what
 * results should be returned in the search. For example:
 *
 * <p><pre class="code">
 *     Filter.equal("name","Paul")
 * </pre>
 *
 * were added to the search, only objects with the property "name" equal to the string "Paul"
 * would be returned. Nested properties can also be specified. For example:
 *
 * <p><pre class="code">
 *     Filter.greaterThan("employee.age", 65)
 * </pre>.
 */
public class Filter implements Serializable {

  private static final long serialVersionUID = 1L;

  /**
   * Property string representing the root entity of the search. This is the empty string ("").
   */
  public static final String ROOT_ENTITY = "";
  private static final FilterOperator DEFAULT_OPERATOR = FilterOperator.OP_EQUAL;

  /**
   * The name of the property to filter on. It may be nested. For example:
   *
   * <p><pre class="code">
   *     "name", "createdDate", "user.age"
   * </pre>
   */
  private String property;

  /**
   * The value to compare the property with. Should be of a compatible type with the property.
   *
   * <p><pre class="code">
   *     "Fred", new Date(), 45
   * </pre>
   */
  private Object value;

  /**
   * The type of enum {@link FilterOperator}.
   */
  private FilterOperator operator;

  /**
   * Creates a new {@link Filter} instance.
   */
  public Filter() { }

  /**
   * Creates a new {@link Filter} instance.
   *
   * @param property must not be {@literal null} or {@literal empty}
   * @param value can be {@literal null}
   */
  public Filter(@Nonnull String property, @Nullable Object value) {
    this(property, value, null);
  }

  /**
   * Creates a new {@link Filter} instance.
   *
   * @param property must not be {@literal null} or {@literal empty}
   * @param value can be {@literal null}
   * @param operator can be {@literal null}
   */
  public Filter(@Nonnull String property, @Nullable Object value, @Nullable FilterOperator operator) {

    //if (!StringUtils.hasText(property)) {
    //    throw new IllegalArgumentException("Property must not be null or empty!");
    //}

    this.property = property;
    this.value = value;
    this.operator = operator == null ? DEFAULT_OPERATOR : operator;
  }

  /**
   * Create a new Filter using the {@literal ==} operator.
   */
  public static @Nonnull Filter equal(@Nonnull String property, Object value) {
    return new Filter(property, value, FilterOperator.OP_EQUAL);
  }

  /**
   * Create a new Filter using the {@literal !=} operator.
   */
  public static @Nonnull Filter notEqual(@Nonnull String property, Object value) {
    return new Filter(property, value, FilterOperator.OP_NOT_EQUAL);
  }

  /**
   * Create a new Filter using the {@literal <} operator.
   */
  public static @Nonnull Filter lessThan(@Nonnull String property, Object value) {
    return new Filter(property, value, FilterOperator.OP_LESS_THAN);
  }

  /**
   * Create a new Filter using the {@literal >} operator.
   */
  public static @Nonnull Filter greaterThan(@Nonnull String property, Object value) {
    return new Filter(property, value, FilterOperator.OP_GREATER_THAN);
  }

  /**
   * Create a new Filter using the {@literal <=} operator.
   */
  public static @Nonnull Filter lessOrEqual(@Nonnull String property, Object value) {
    return new Filter(property, value, FilterOperator.OP_LESS_OR_EQUAL);
  }

  /**
   * Create a new Filter using the {@literal >=} operator.
   */
  public static @Nonnull Filter greaterOrEqual(@Nonnull String property, Object value) {
    return new Filter(property, value, FilterOperator.OP_GREATER_OR_EQUAL);
  }

  /**
   * Create a new Filter using the {@literal IN} operator.
   */
  public static @Nonnull Filter in(@Nonnull String property, Collection<?> value) {
    return new Filter(property, value, FilterOperator.OP_IN);
  }

  /**
   * Create a new Filter using the {@literal IN} operator.
   */
  public static @Nonnull Filter in(@Nonnull String property, Object... value) {
    return new Filter(property, value, FilterOperator.OP_IN);
  }

  /**
   * Create a new Filter using the {@literal NOT IN} operator.
   */
  public static @Nonnull Filter notIn(@Nonnull String property, Collection<?> value) {
    return new Filter(property, value, FilterOperator.OP_NOT_IN);
  }

  /**
   * Create a new Filter using the {@literal NOT IN} operator.
   */
  public static @Nonnull Filter notIn(@Nonnull String property, Object... value) {
    return new Filter(property, value, FilterOperator.OP_NOT_IN);
  }

  /**
   * Create a new Filter using the {@literal LIKE} operator.
   */
  public static @Nonnull Filter like(@Nonnull String property, String value) {
    return new Filter(property, value, FilterOperator.OP_LIKE);
  }

  /**
   * Create a new Filter using the {@literal ILIKE} operator.
   */
  public static @Nonnull Filter ilike(@Nonnull String property, String value) {
    return new Filter(property, value, FilterOperator.OP_ILIKE);
  }

  /**
   * Create a new Filter using the {@literal IS NULL} operator.
   */
  public static @Nonnull Filter isNull(@Nonnull String property) {
    return new Filter(property, true, FilterOperator.OP_NULL);
  }

  /**
   * Create a new Filter using the {@literal IS NOT NULL} operator.
   */
  public static @Nonnull Filter isNotNull(@Nonnull String property) {
    return new Filter(property, true, FilterOperator.OP_NOT_NULL);
  }

  /**
   * Create a new Filter using the {@literal IS EMPTY} operator.
   */
  public static @Nonnull Filter isEmpty(@Nonnull String property) {
    return new Filter(property, true, FilterOperator.OP_EMPTY);
  }

  /**
   * Create a new Filter using the {@literal IS NOT EMPTY} operator.
   */
  public static @Nonnull Filter isNotEmpty(@Nonnull String property) {
    return new Filter(property, true, FilterOperator.OP_NOT_EMPTY);
  }

  /**
   * Create a new Filter using the {@literal AND} operator.
   */
  public static @Nonnull Filter and(@Nonnull Filter... filters) {
    Filter filter = new Filter("AND", null, FilterOperator.OP_AND);
    for (Filter f : filters) {
      filter.add(f);
    }
    return filter;
  }

  /**
   * Create a new Filter using the {@literal OR} operator.
   */
  public static @Nonnull Filter or(Filter... filters) {
    Filter filter = and(filters);
    filter.property = "OR";
    filter.operator = FilterOperator.OP_OR;
    return filter;
  }

  /**
   * Create a new Filter using the {@literal NOT} operator.
   */
  public static @Nonnull Filter not(Filter filter) {
    return new Filter("NOT", filter, FilterOperator.OP_NOT);
  }

  /**
   * Create a new Filter using the {@literal SOME} operator.
   */
  public static @Nonnull Filter some(@Nonnull String property, Filter filter) {
    return new Filter(property, filter, FilterOperator.OP_SOME);
  }

  /**
   * Create a new Filter using the {@literal NONE} operator (equivalent to {@literal NOT SOME}).
   */
  public static @Nonnull Filter none(@Nonnull String property, Filter filter) {
    return new Filter(property, filter, FilterOperator.OP_NONE);
  }

  /**
   * Create a new Filter using the {@literal ALL} operator.
   */
  public static @Nonnull Filter all(@Nonnull String property, Filter filter) {
    return new Filter(property, filter, FilterOperator.OP_ALL);
  }

  /**
   * Create a new Filter using a custom JPQL/HQL expression. This can be any valid where-clause
   * type expression. Reference properties by wrapping them with curly braces ({}). For examples:
   *
   * <p><pre class="code">
   *     // Referencing a property in a custom expression
   *     Filter.custom("{serialNumber} like '%4780%'");
   *     // Comparing two properties
   *     Filter.custom("{parent.spotCount} > {spotCount} + 4");
   *     // A constant
   *     Filter.custom("1 = 1");
   *     // A function
   *     Filter.custom("{dueDate} > current_date()");
   *     // A subquery
   *     Filter.custom("{id} in (
   *         select pc.cat_id
   *         from popular_cats pc
   *         where pc.color = 'blue'
   *     )");
   * </pre>
   *
   * @param expression JPQL/HQL where-clause expression
   */
  public static @Nonnull Filter custom(String expression) {
    return new Filter(expression, null, FilterOperator.OP_CUSTOM);
  }

  /**
   * Create a new Filter using a custom JPQL/HQL expression. This can be any valid where-clause
   * type expression. Reference properties by wrapping them with curly braces ({}).
   * The expression can also contain place holders for the {@link Filter} values; these are
   * indicated by JPQL-style positional parameters (i.e. a question mark (?) followed by a
   * number indicating the parameter order, starting with one). For examples:
   *
   * <p><pre class="code">
   *     // Referencing a property in a custom expression
   *     Filter.custom("{serialNumber} like ?1", "%4780%");
   *     // comparing two properties
   *     Filter.custom("{parent.spotCount} + ?1 > {spotCount} + ?2", 0, 4);
   *     // A constant
   *     Filter.custom("?1 = ?2", 1, 1);
   *     // A function
   *     Filter.custom("?1 > current_date()", someDate);
   *     // A subquery
   *     Filter.custom("{id} in (
   *         select pc.cat_id
   *         from popular_cats pc
   *         where pc.color = ?1
   *     )", "blue");
   * </pre>
   *
   * @param expression JPQL/HQL where-clause expression.
   * @param values one or more values to fill in the numbered placeholders in the expression.
   */
  public static @Nonnull Filter custom(String expression, Object... values) {
    return new Filter(expression, values, FilterOperator.OP_CUSTOM);
  }

  /**
   * Create a new Filter using a custom JPQL/HQL expression. This can be any valid where-clause
   * type expression. Reference properties by wrapping them with curly braces ({}).
   * The expression can also contain place holders for the {@link Filter} values; these are
   * indicated by JPQL-style positional parameters (i.e. a question mark (?) followed by a
   * number indicating the parameter order, starting with one). For examples:
   *
   * <p><pre class="code">
   *     // Referencing a property in a custom expression
   *     Filter.custom("{serialNumber} like ?1", Collections.singleton("%4780%"));
   *     // comparing two properties
   *     Filter.custom("{parent.spotCount} + ?1 > {spotCount} + ?2", Arrays.asList(0, 4));
   *     // A constant
   *     Filter.custom("?1 = ?2", Arrays.asList(1, 1));
   *     // A function
   *     Filter.custom("?1 > current_date()", Collections.singleton(someDate));
   *     // A subquery
   *     Filter.custom("{id} in (
   *         select pc.cat_id
   *         from popular_cats pc
   *         where pc.color = ?1
   *     )", Collections.singleton("blue"));
   * </pre>
   *
   * @param expression JPQL/HQL where-clause expression
   * @param values one or more values to fill in the numbered placeholders in the expression
   */
  public static @Nonnull Filter custom(String expression, Collection<?> values) {
    return new Filter(expression, values, FilterOperator.OP_CUSTOM);
  }

  /**
   * Used with {@literal OP_OR} and {@literal OP_AND} filters. These filters take a collection
   * of filters as their value. This method adds a filter to that list.
   */
  @SuppressWarnings("unchecked")
  public void add(Filter filter) {
    if (value == null || !(value instanceof List)) {
      value = new ArrayList<>();
    }
    ((List<Object>) value).add(filter);
  }

  /**
   * Used with {@literal OP_OR} and {@literal OP_AND} filters. These filters take a collection
   * of filters as their value. This method removes a filter from that list.
   */
  @SuppressWarnings("unchecked")
  public void remove(Filter filter) {
    if (value == null || !(value instanceof List)) {
      return;
    }
    ((List<Object>) value).remove(filter);
  }

  /**
   * Returns the property for filter.
   *
   * @return the property
   */
  public String getProperty() {
    return property;
  }

  /**
   * Property by this filter.
   */
  public void setProperty(String property) {
    this.property = property;
  }

  /**
   * Returns the value for the property shall be filtered to.
   */
  public Object getValue() {
    return value;
  }

  /**
   * The value for the property.
   */
  public void setValue(Object value) {
    this.value = value;
  }

  /**
   * Returns the operator to apply to the column.
   */
  public FilterOperator getOperator() {
    return operator;
  }

  /**
   * The operator to apply to the column.
   */
  public void setOperator(FilterOperator operator) {
    this.operator = operator;
  }

  /**
   * Returns the value as a List, converting if necessary. If the value is a List, it will be
   * returned directly. If it is any other {@link Collection} type or if it is an Array, an
   * {@link ArrayList} will be returned with all the same elements. If the value is any other
   * non-null value, a {@link List} containing just that one value will be returned. If it is
   * {@literal null}, will be returned {@literal null}.
   *
   * @return List
   */
  public List<?> getValuesAsList() {
    if (value == null) {
      return null;
    }
    else if (value instanceof List<?>) {
      return (List<?>) value;
    }
    else if (value instanceof Collection<?>) {
      return new ArrayList<Object>((Collection<?>) value);
    }
    else if (value.getClass().isArray()) {
      ArrayList<Object> list = new ArrayList<>(Array.getLength(value));
      for (int i = 0; i < Array.getLength(value); i++) {
        list.add(Array.get(value, i));
      }
      return list;
    }
    else {
      return Collections.singletonList(value);
    }
  }

  /**
   * Returns the {@literal value} as a type of {@link Collection}, converting if necessary.
   * If the {@literal value} is a type of {@link Collection}, it will be returned directly.
   * If the {@literal value} is an {@link Array}, will be  returned an {@link ArrayList} with
   * all the same elements. If the {@literal value} is any other non-null value, a {@link Set}
   * containing just that one value will be returned. If the {@literal value} is {@literal null},
   * {@literal null} will be returned.
   *
   * @return Collection
   */
  public Collection<?> getValuesAsCollection() {
    if (value == null) {
      return null;
    }
    else if (value instanceof Collection<?>) {
      return (Collection<?>) value;
    }
    else if (value.getClass().isArray()) {
      ArrayList<Object> list = new ArrayList<>(Array.getLength(value));
      for (int i = 0; i < Array.getLength(value); i++) {
        list.add(Array.get(value, i));
      }
      return list;
    }
    else {
      return Collections.singleton(value);
    }
  }

  /**
   * @return true if the operator should have a single value specified.
   *
   * <p><pre class="code">
   *     EQUAL, NOT_EQUAL, LESS_THAN, LESS_OR_EQUAL, GREATER_THAN, GREATER_OR_EQUAL, LIKE, ILIKE
   * </pre>
   */
  public boolean isTakesSingleValue() {
    return operator == FilterOperator.OP_EQUAL
        || operator == FilterOperator.OP_NOT_EQUAL
        || operator == FilterOperator.OP_LESS_THAN
        || operator == FilterOperator.OP_GREATER_THAN
        || operator == FilterOperator.OP_LESS_OR_EQUAL
        || operator == FilterOperator.OP_GREATER_OR_EQUAL
        || operator == FilterOperator.OP_LIKE
        || operator == FilterOperator.OP_ILIKE;
  }

  /**
   * @return true if the operator should have a list of values specified.
   *
   * <p><pre class="code">
   *     IN, NOT_IN
   * </pre>
   */
  public boolean isTakesListOfValues() {
    return operator == FilterOperator.OP_IN
        || operator == FilterOperator.OP_NOT_IN;
  }

  /**
   * @return true if the operator does not require a value to be specified.
   *
   * <p><pre class="code">
   *     NULL, NOT_NULL, EMPTY, NOT_EMPTY
   * </pre>
   */
  public boolean isTakesNoValue() {
    return operator == FilterOperator.OP_NULL
        || operator == FilterOperator.OP_NOT_NULL
        || operator == FilterOperator.OP_EMPTY
        || operator == FilterOperator.OP_NOT_EMPTY
        || operator == FilterOperator.OP_CUSTOM;
  }

  /**
   * @return true if the operator should have a single Filter specified for the value.
   *
   * <p><pre class="code">
   *     NOT, SOME, NONE, ALL
   * </pre>
   */
  public boolean isTakesSingleSubFilter() {
    return operator == FilterOperator.OP_NOT
        || operator == FilterOperator.OP_SOME
        || operator == FilterOperator.OP_NONE
        || operator == FilterOperator.OP_ALL;
  }

  /**
   * @return true if the operator should have a list of Filters specified for the value.
   *
   * <p><pre class="code">
   *     AND, OR
   * </pre>
   */
  public boolean isTakesListOfSubFilters() {
    return operator == FilterOperator.OP_AND
        || operator == FilterOperator.OP_OR;
  }

  /**
   * @return true if the operator does not require a property to be specified.
   *
   * <p><pre class="code">
   *     AND, OR, NOT
   * </pre>
   */
  public boolean isTakesNoProperty() {
    return operator == FilterOperator.OP_AND
        || operator == FilterOperator.OP_OR
        || operator == FilterOperator.OP_NOT;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + property.hashCode();
    result = prime * result + ((value == null) ? 0 : value.hashCode());
    result = prime * result + ((operator == null) ? 0 : operator.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;

    Filter other = (Filter) obj;

    if (operator != other.operator)
      return false;
    if (property == null) {
      if (other.property != null)
        return false;
    } else if (!property.equals(other.property))
      return false;
    if (value == null) {
      return other.value == null;
    } else return value.equals(other.value);
  }

  @Override
  @SuppressWarnings("unchecked")
  public String toString() {
    switch (operator) {
      case OP_IN:
        return "`" + property + "` in (" + TypeCheckUtils.paramDisplayString(value) + ")";
      case OP_NOT_IN:
        return "`" + property + "` not in (" + TypeCheckUtils.paramDisplayString(value) + ")";
      case OP_EQUAL:
        return "`" + property + "` = " + TypeCheckUtils.paramDisplayString(value);
      case OP_NOT_EQUAL:
        return "`" + property + "` != " + TypeCheckUtils.paramDisplayString(value);
      case OP_GREATER_THAN:
        return "`" + property + "` > " + TypeCheckUtils.paramDisplayString(value);
      case OP_LESS_THAN:
        return "`" + property + "` < " + TypeCheckUtils.paramDisplayString(value);
      case OP_GREATER_OR_EQUAL:
        return "`" + property + "` >= " + TypeCheckUtils.paramDisplayString(value);
      case OP_LESS_OR_EQUAL:
        return "`" + property + "` <= " + TypeCheckUtils.paramDisplayString(value);
      case OP_LIKE:
        return "`" + property + "` LIKE " + TypeCheckUtils.paramDisplayString(value);
      case OP_ILIKE:
        return "`" + property + "` ILIKE " + TypeCheckUtils.paramDisplayString(value);
      case OP_NULL:
        return "`" + property + "` IS NULL";
      case OP_NOT_NULL:
        return "`" + property + "` IS NOT NULL";
      case OP_EMPTY:
        return "`" + property + "` IS EMPTY";
      case OP_NOT_EMPTY:
        return "`" + property + "` IS NOT EMPTY";
      case OP_AND:
      case OP_OR:
        if (!(value instanceof List)) {
          return (operator == FilterOperator.OP_AND ? "AND: " : "OR: ") + "**INVALID VALUE - NOT A LIST: (" + value
              + ") **";
        }

        String op = operator == FilterOperator.OP_AND ? " and " : " or ";

        StringBuilder sb = new StringBuilder("(");
        boolean first = true;

        for (Object obj : ((List<Object>) value)) {
          if (first) {
            first = false;
          } else {
            sb.append(op);
          }
          if (obj instanceof Filter) {
            sb.append(obj.toString());
          } else {
            sb.append("**INVALID VALUE - NOT A FILTER: (").append(obj).append(") **");
          }
        }

        if (first)
          return (operator == FilterOperator.OP_AND ? "AND: " : "OR: ") + "**EMPTY LIST**";

        sb.append(")");
        return sb.toString();
      case OP_NOT:
        if (!(value instanceof Filter)) {
          return "NOT: **INVALID VALUE - NOT A FILTER: (" + value + ") **";
        }
        return "not " + value.toString();
      case OP_SOME:
        if (!(value instanceof Filter)) {
          return "SOME: **INVALID VALUE - NOT A FILTER: (" + value + ") **";
        }
        return "some `" + property + "` {" + value.toString() + "}";
      case OP_NONE:
        if (!(value instanceof Filter)) {
          return "NONE: **INVALID VALUE - NOT A FILTER: (" + value + ") **";
        }
        return "none `" + property + "` {" + value.toString() + "}";
      case OP_ALL:
        if (!(value instanceof Filter)) {
          return "ALL: **INVALID VALUE - NOT A FILTER: (" + value + ") **";
        }
        return "all `" + property + "` {" + value.toString() + "}";
      case OP_CUSTOM:
        if (value == null || (value instanceof Collection && ((Collection<Object>) value).isEmpty()) || (value.getClass().isArray() && Array.getLength(value) == 0)) {
          return "CUSTOM[" + property + "]";
        } else {
          StringBuilder sb2 = new StringBuilder();
          sb2.append("CUSTOM[").append(property).append("]values(");
          //boolean first2 = true;
          if (value instanceof Collection) {
            //if (first2) {
            //    first2 = false;
            //} else {
            //    sb2.append(',');
            //}
            for (Object o : (Collection<Object>) value) {
              sb2.append(o);
            }
          } else if (value.getClass().isArray()) {
            //if (first2) {
            //    first2 = false;
            //} else {
            //    sb2.append(',');
            //}
            for (int i = 0; i < Array.getLength(value); i++) {
              sb2.append(Array.get(value, i));
            }
          } else {
            sb2.append(value);
          }
          sb2.append(")");
        }
      default:
        return "**INVALID OPERATOR: (" + operator + ") - VALUE: " + TypeCheckUtils.paramDisplayString(value) + " **";
    }
  }

}
