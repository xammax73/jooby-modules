package io.xammax.jooby.hibernate;

import io.xammax.jooby.hibernate.search.Metadata;
import io.xammax.jooby.hibernate.search.MetadataUtils;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.metadata.ClassMetadata;
import org.hibernate.metamodel.spi.MetamodelImplementor;
import org.hibernate.proxy.HibernateProxyHelper;

import javax.annotation.Nonnull;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class HibernateMetadataUtils implements MetadataUtils {

  private static final Map<SessionFactory, HibernateMetadataUtils> map = new HashMap<>();
  private SessionFactory sessionFactory;

  public static @Nonnull HibernateMetadataUtils getInstanceForSessionFactory(SessionFactory sessionFactory) {
    HibernateMetadataUtils instance = map.get(sessionFactory);
    if (instance == null) {
      instance = new HibernateMetadataUtils();
      instance.sessionFactory = sessionFactory;
      map.put(sessionFactory, instance);
    }
    return instance;
  }

  protected HibernateMetadataUtils() { }

  // Public methods

  public Serializable getId(Object entity) {
    if (entity == null)
      throw new NullPointerException("Cannot get ID from null object.");
    return get(entity.getClass()).getIdValue(entity);
  }

  public boolean isId(Class<?> rootClass, String propertyPath) {
    if (propertyPath == null || "".equals(propertyPath))
      return false;
    // with hibernate, "id" always refers to the id property, no matter what
    // that property is named. just make sure the segment before this "id"
    // refers to an entity since only entities have ids.
    if (propertyPath.equals("id") ||
        (propertyPath.endsWith(".id") && get(rootClass, propertyPath.substring(0, propertyPath.length() - 3)).isEntity()))
      return true;
    // see if the property is the identifier property of the entity it belongs to.
    int pos = propertyPath.lastIndexOf(".");
    if (pos != -1) {
      Metadata parentType = get(rootClass, propertyPath.substring(0, pos));
      if (!parentType.isEntity())
        return false;
      return propertyPath.substring(pos + 1).equals(parentType.getIdProperty());
    } else {
      MetamodelImplementor metamodel = (MetamodelImplementor) sessionFactory.getMetamodel();
      ClassMetadata classMetadata = (ClassMetadata) metamodel.entityPersister(rootClass);
      return propertyPath.equals(classMetadata.getIdentifierPropertyName());
    }
  }

  public Metadata get(Class<?> entityClass) throws IllegalArgumentException {
    entityClass = getUnproxiedClass(entityClass);
    MetamodelImplementor metamodel = (MetamodelImplementor) sessionFactory.getMetamodel();
    ClassMetadata classMetadata = (ClassMetadata) metamodel.entityPersister(entityClass);
    if (classMetadata == null) {
      throw new IllegalArgumentException("Unable to introspect " + entityClass.toString()
          + ". The class is not a registered Hibernate entity.");
    } else {
      return new HibernateEntityMetadata(sessionFactory, classMetadata, null);
    }
  }

  public Metadata get(Class<?> rootEntityClass, String propertyPath) throws IllegalArgumentException {
    try {
      Metadata metadata = get(rootEntityClass);
      if (propertyPath == null || "".equals(propertyPath)) {
        return metadata;
      }
      String[] chain = propertyPath.split("\\.");
      for (String s : chain) {
        metadata = metadata.getPropertyType(s);
      }
      return metadata;
    } catch (HibernateException ex) {
      throw new IllegalArgumentException("Could not find property '" + propertyPath + "' on class "
          + rootEntityClass + ".");
    }
  }

  @SuppressWarnings("unchecked")
  public <T> Class<T> getUnproxiedClass(Class<?> cls) {
    // classMetadata will be null if entityClass is not registered with Hibernate or
    // when it is a Hibernate proxy class. So if a class is not recognized, we will look
    // at superclasses to see if it is a proxy.
    MetamodelImplementor metamodel = (MetamodelImplementor) sessionFactory.getMetamodel();
    while (metamodel.entityPersister(cls) == null) {
      cls = cls.getSuperclass();
      if (Object.class.equals(cls))
        return null;
    }
    return (Class<T>) cls;
  }

  @SuppressWarnings("unchecked")
  public <T> Class<T> getUnproxiedClass(Object entity) {
    return HibernateProxyHelper.getClassWithoutInitializingProxy(entity);
  }

}
