package io.xammax.jooby.hibernate.search;

import java.io.Serializable;
import java.util.List;

public class SearchResult<RT> implements Serializable {

  private static final long serialVersionUID = 1L;

  protected List<RT> result;
  protected long totalCount = -1L;

  /**
   * The results of the search.
   */
  public List<RT> getResult() {
    return result;
  }

  /**
   * The results of the search.
   */
  public void setResult(List<RT> results) {
    this.result = results;
  }

  /**
   * Gets the total number of results that would have been returned if no {@literal maxResults}
   * had been specified. (-1 means unspecified.)
   */
  public long getTotalCount() {
    return totalCount;
  }

  /**
   * Sets the total number of results that would have been returned if no {@literal maxResults}
   * had been specified. (-1 means unspecified.)
   */
  public void setTotalCount(long totalCount) {
    this.totalCount = totalCount;
  }

}
