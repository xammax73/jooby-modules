package io.xammax.jooby.hibernate.model;

import com.google.common.base.MoreObjects;
import org.hibernate.proxy.HibernateProxyHelper;

import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import java.io.Serializable;

/**
 * Abstract base class for entities. Allows parameterization of id type and implements
 * {@code #equals({@link Object})} and {@code #hashCode()} based on that id.
 *
 * @param <ID> the type of the entity identifier
 */
@MappedSuperclass
public abstract class AbstractPersistable<ID extends Serializable> extends AbstractIdentifiable<ID> implements Persistable<ID> {

  private static final long serialVersionUID = 9149914419520367894L;

  /**
   * Must be {@link Transient} in order to ensure that no JPA provider complains because of a
   * missing setter.
   *
   * @see Persistable#isNew()
   */
  @Override
  @Transient
  public boolean isNew() {
    return null == getId();
  }

  @Override
  @SuppressWarnings("unchecked")
  public boolean equals(Object obj) {
    if (null == obj) {
      return false;
    }
    if (this == obj) {
      return true;
    }
    if (!(obj instanceof AbstractPersistable)) {
      return false;
    }
    if (getId() == null || ((AbstractPersistable<?>) obj).getId() == null) {
      return false;
    }
    if (!getId().equals(((AbstractPersistable<?>) obj).getId())) {
      return false;
    }
    return HibernateProxyHelper.getClassWithoutInitializingProxy(obj).isAssignableFrom(this.getClass());
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
    return result;
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() +
        (getId() != null ? MoreObjects.toStringHelper(this).add(", id = ", getId()).toString() : "");
  }

}
