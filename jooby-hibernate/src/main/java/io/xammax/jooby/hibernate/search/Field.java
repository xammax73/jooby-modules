package io.xammax.jooby.hibernate.search;

import io.xammax.jooby.util.StringUtils;

import javax.annotation.Nullable;
import java.io.Serializable;

public class Field implements Serializable {

  private static final long serialVersionUID = 1L;

  /**
   * Property string representing the root entity of the search. This is just the empty string.
   */
  public static final String ROOT_ENTITY = "";

  /**
   * Property string representing default key. This is just the empty string.
   */
  private static final String DEFAULT_KEY = "";

  /**
   * Sets the property in {@code DEFAULT_OPERATOR}.
   */
  private static final FieldOperator DEFAULT_OPERATOR = FieldOperator.OP_PROPERTY;

  /**
   * The property to include in the result.
   */
  private String property;

  /**
   * The key to use for the property when using result mode ({@code RESULT_MAP}.
   */
  private String key;

  /**
   * The type of enum {@link FieldOperator}.
   */
  private FieldOperator operator;

  /**
   * Creates a new {@link Field} instance.
   */
  public Field() { }

  /**
   * Creates a new {@link Field} instance.
   *
   * @param property must not be {@literal null} or {@literal empty}
   */
  public Field(String property) {
    this(property, null, null);
  }

  /**
   * Creates a new {@link Field} instance.
   *
   * @param property must not be {@literal null} or {@literal empty}
   * @param key can be {@literal null}
   */
  public Field(String property, String key) {
    this(property, key, null);
  }

  /**
   * Creates a new {@link Field} instance.
   *
   * @param property must not be {@literal null} or {@literal empty}
   * @param operator can be {@literal null}
   */
  public Field(String property, FieldOperator operator) {
    this(property, null, operator);
  }

  /**
   * Creates a new {@link Field} instance.
   *
   * @param property must not be {@literal null} or {@literal empty}
   * @param key can be {@literal null}
   * @param operator can be {@literal null}
   */
  public Field(String property, @Nullable String key, @Nullable FieldOperator operator) {

    if (!StringUtils.hasText(property)) {
      throw new IllegalArgumentException("Property must not be null or empty!");
    }

    this.property = property;
    this.key = key == null ? DEFAULT_KEY : key;
    this.operator = operator == null ? DEFAULT_OPERATOR : operator;
  }

  /**
   * Returns the property for field.
   *
   * @return the property
   */
  public String getProperty() {
    return property;
  }

  /**
   * Property by this field.
   */
  public void setProperty(String property) {
    this.property = property;
  }

  /**
   * Returns the key to use for the property using result mode {@literal RESULT_MAP}.
   */
  public String getKey() {
    return key;
  }

  /**
   * The key to use for the property.
   */
  public void setKey(String key) {
    this.key = key;
  }

  /**
   * Returns the operator to apply to the column.
   */
  public FieldOperator getOperator() {
    return operator;
  }

  /**
   * The operator to apply to the column.
   */
  public void setOperator(FieldOperator operator) {
    this.operator = operator;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;

    Field other = (Field) obj;

    if (key == null) {
      if (other.key != null)
        return false;
    } else if (!key.equals(other.key))
      return false;
    if (operator != other.operator)
      return false;
    if (property == null) {
      return other.property == null;
    } else return property.equals(other.property);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((operator == null) ? 0 : operator.hashCode());
    result = prime * result + ((key == null) ? 0 : key.hashCode());
    result = prime * result + ((property == null) ? 0 : property.hashCode());
    return result;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    boolean parens = true;

    switch (operator) {
      case OP_AVG: sb.append("AVG("); break;
      case OP_COUNT: sb.append("COUNT("); break;
      case OP_COUNT_DISTINCT: sb.append("COUNT_DISTINCT("); break;
      case OP_MAX: sb.append("MAX("); break;
      case OP_MIN: sb.append("MIN("); break;
      case OP_PROPERTY: parens = false; break;
      case OP_SUM: sb.append("SUM("); break;
      case OP_CUSTOM: sb.append("CUSTOM: "); parens = false; break;
      //default:  sb.append("**INVALID OPERATOR: (" + operator + ")** "); parens = false; break;
      default:  sb.append("**INVALID OPERATOR: (").append(operator).append(")** "); parens = false; break;
    }

    if (property == null) {
      sb.append("null");
    } else {
      sb.append("`");
      sb.append(property);
      sb.append("`");
    }
    if (parens)
      sb.append(")");

    if (key != null) {
      sb.append(" as `");
      sb.append(key);
      sb.append("`");
    }

    return sb.toString();
  }

}
