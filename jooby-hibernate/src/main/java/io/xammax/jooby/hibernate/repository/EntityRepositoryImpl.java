package io.xammax.jooby.hibernate.repository;

import io.jooby.ServiceKey;
import io.jooby.SneakyThrows;
import io.jooby.hibernate.SessionProvider;
import io.xammax.jooby.util.ReflectionUtils;
import io.xammax.jooby.hibernate.model.Persistable;
import io.xammax.jooby.hibernate.HibernateMetadataUtils;
import io.xammax.jooby.hibernate.HibernateSearchProcessor;
import io.xammax.jooby.hibernate.search.ImmutableSearch;
import io.xammax.jooby.hibernate.search.Search;
import io.xammax.jooby.hibernate.search.SearchResult;
import org.hibernate.NonUniqueResultException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.context.internal.ManagedSessionContext;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static io.xammax.jooby.JoobyInstance.app;

public class EntityRepositoryImpl<E extends Persistable<ID>, ID extends Serializable> implements EntityRepository<E, ID> {

  protected final static Logger logger = LoggerFactory.getLogger(EntityRepositoryImpl.class);

  protected final HibernateMetadataUtils metadataUtils;
  protected final HibernateSearchProcessor searchProcessor;
  protected ServiceKey<SessionFactory> sessionFactoryKey;
  protected ServiceKey<SessionProvider> sessionProviderKey;
  protected EntityManagerFactory entityManagerFactory;
  protected SessionFactory sessionFactory;
  protected Class<E> persistentClass;

  @SuppressWarnings("unchecked")
  public EntityRepositoryImpl() {
    this.entityManagerFactory = app.require(EntityManagerFactory.class);
    this.sessionFactoryKey = ServiceKey.key(SessionFactory.class, "db.default");
    this.sessionProviderKey = sessionFactoryKey.getName() == null
        ? ServiceKey.key(SessionProvider.class)
        : ServiceKey.key(SessionProvider.class, sessionFactoryKey.getName());
    this.sessionFactory = app.require(sessionFactoryKey);
    this.metadataUtils = HibernateMetadataUtils.getInstanceForSessionFactory(sessionFactory);
    this.searchProcessor = HibernateSearchProcessor.getInstanceForSessionFactory(sessionFactory);
    try {
      persistentClass = (Class<E>) ReflectionUtils.getTypeArguments(EntityRepositoryImpl.class, this.getClass()).get(0);
    } catch (Throwable e) {
      logger.error("Error to detect crud class.", e);
    }
  }

  /**
   * Gets the EntityManager sessionFactory
   *
   * @return EntityManager sessionFactory
   */
  protected EntityManagerFactory getEntityManagerFactory() {
    return entityManagerFactory;
  }

  /**
   * Gets the Hibernate sessionFactory
   *
   * @return Hibernate sessionFactory
   */
  protected SessionFactory getSessionFactory() {
    return sessionFactory;
  }

  /**
   * Gets the current Entity manager
   *
   * @return current Entity manager
   */
  protected EntityManager getEntityManager() {
    return getEntityManagerFactory().createEntityManager();
  }

  /**
   * Gets the current Hibernate session
   *
   * @return current Hibernate session
   */
  protected Session getSession() {
    return getSessionFactory().getCurrentSession();
  }

  /**
   * Get the instance of HibernateMetadataUtils associated with the session factory.
   *
   * @return current state of metadataUtils
   */
  protected HibernateMetadataUtils getMetadataUtils() {
    return metadataUtils;
  }

  protected HibernateSearchProcessor getSearchProcessor() {
    return searchProcessor;
  }

  // Public methods

  public void delete(@Nonnull E entity) {
    SessionFactory sessionFactory = app.require(sessionFactoryKey);
    SessionProvider sessionProvider = app.require(sessionProviderKey);
    try (Session session = sessionProvider.newSession(sessionFactory.withOptions())) {
      ManagedSessionContext.bind(session);
      Transaction trx = null;
      try {
        trx = session.getTransaction();
        trx.begin();
        _delete(session, entity);
        if (trx.isActive()) {
          trx.commit();
        }
      } catch (Throwable ex) {
        if (trx != null && trx.isActive()) {
          trx.rollback();
        }
        assert false;
        throw SneakyThrows.propagate(ex);
      }
      //ensureCompletion(session.getTransaction());
    } finally {
      ManagedSessionContext.unbind(sessionFactory);
    }
  }

  public void deleteAll(@Nonnull Iterable<? extends E> entities) {
    SessionFactory sessionFactory = app.require(sessionFactoryKey);
    SessionProvider sessionProvider = app.require(sessionProviderKey);
    try (Session session = sessionProvider.newSession(sessionFactory.withOptions())) {
      ManagedSessionContext.bind(session);
      Transaction trx = null;
      try {
        trx = session.getTransaction();
        trx.begin();
        _delete(session, entities);
        if (trx.isActive()) {
          trx.commit();
        }
      } catch (Throwable ex) {
        if (trx != null && trx.isActive()) {
          trx.rollback();
        }
        assert false;
        throw SneakyThrows.propagate(ex);
      }
      //ensureCompletion(session.getTransaction());
    } finally {
      ManagedSessionContext.unbind(sessionFactory);
    }
  }

  public void deleteById(@Nonnull ID id) {
    SessionFactory sessionFactory = app.require(sessionFactoryKey);
    SessionProvider sessionProvider = app.require(sessionProviderKey);
    try (Session session = sessionProvider.newSession(sessionFactory.withOptions())) {
      ManagedSessionContext.bind(session);
      Transaction trx = null;
      try {
        trx = session.getTransaction();
        trx.begin();
        _deleteById(session, persistentClass, id);
        if (trx.isActive()) {
          trx.commit();
        }
      } catch (Throwable ex) {
        if (trx != null && trx.isActive()) {
          trx.rollback();
        }
        assert false;
        throw SneakyThrows.propagate(ex);
      }
      //ensureCompletion(session.getTransaction());
    } finally {
      ManagedSessionContext.unbind(sessionFactory);
    }
  }

  public void deleteByIds(@Nonnull Iterable<? extends ID> ids) {
    SessionFactory sessionFactory = app.require(sessionFactoryKey);
    SessionProvider sessionProvider = app.require(sessionProviderKey);
    try (Session session = sessionProvider.newSession(sessionFactory.withOptions())) {
      ManagedSessionContext.bind(session);
      Transaction trx = null;
      try {
        trx = session.getTransaction();
        trx.begin();
        _deleteById(session, persistentClass, ids);
        if (trx.isActive()) {
          trx.commit();
        }
      } catch (Throwable ex) {
        if (trx != null && trx.isActive()) {
          trx.rollback();
        }
        assert false;
        throw SneakyThrows.propagate(ex);
      }
      //ensureCompletion(session.getTransaction());
    } finally {
      ManagedSessionContext.unbind(sessionFactory);
    }
  }

  public boolean existsById(@Nonnull ID id) {
    SessionFactory sessionFactory = app.require(sessionFactoryKey);
    SessionProvider sessionProvider = app.require(sessionProviderKey);
    try (Session session = sessionProvider.newSession(sessionFactory.withOptions())) {
      ManagedSessionContext.bind(session);
      Transaction trx = null;
      try {
        trx = session.getTransaction();
        trx.begin();
        boolean result = _existsById(session, persistentClass, id);
        if (trx.isActive()) {
          trx.commit();
        }
        return result;
      } catch (Throwable ex) {
        if (trx != null && trx.isActive()) {
          trx.rollback();
        }
        assert false;
        throw SneakyThrows.propagate(ex);
      }
      //ensureCompletion(session.getTransaction());
    } finally {
      ManagedSessionContext.unbind(sessionFactory);
    }
  }

  public @Nonnull List<E> findAll() {
    SessionFactory sessionFactory = app.require(sessionFactoryKey);
    SessionProvider sessionProvider = app.require(sessionProviderKey);
    try (Session session = sessionProvider.newSession(sessionFactory.withOptions())) {
      ManagedSessionContext.bind(session);
      Transaction trx = null;
      try {
        trx = session.getTransaction();
        trx.begin();
        List<E> result = _all(session, persistentClass);
        if (trx.isActive()) {
          trx.commit();
        }
        return result;
      } catch (Throwable ex) {
        if (trx != null && trx.isActive()) {
          trx.rollback();
        }
        assert false;
        throw SneakyThrows.propagate(ex);
      }
      //ensureCompletion(session.getTransaction());
    } finally {
      ManagedSessionContext.unbind(sessionFactory);
    }
  }

  public @Nonnull Optional<E> findById(@Nonnull ID id) {
    SessionFactory sessionFactory = app.require(sessionFactoryKey);
    SessionProvider sessionProvider = app.require(sessionProviderKey);
    try (Session session = sessionProvider.newSession(sessionFactory.withOptions())) {
      ManagedSessionContext.bind(session);
      Transaction trx = null;
      try {
        trx = session.getTransaction();
        trx.begin();
        Optional<E> result = Optional.ofNullable(_get(session, persistentClass, id));
        if (trx.isActive()) {
          trx.commit();
        }
        return result;
      } catch (Throwable ex) {
        if (trx != null && trx.isActive()) {
          trx.rollback();
        }
        assert false;
        throw SneakyThrows.propagate(ex);
      }
      //ensureCompletion(session.getTransaction());
    } finally {
      ManagedSessionContext.unbind(sessionFactory);
    }
  }

  public @Nonnull List<E> findByIds(@Nonnull Iterable<ID> ids) {
    SessionFactory sessionFactory = app.require(sessionFactoryKey);
    SessionProvider sessionProvider = app.require(sessionProviderKey);
    try (Session session = sessionProvider.newSession(sessionFactory.withOptions())) {
      ManagedSessionContext.bind(session);
      Transaction trx = null;
      try {
        trx = session.getTransaction();
        trx.begin();
        List<E> result = _get(session, persistentClass, ids);
        if (trx.isActive()) {
          trx.commit();
        }
        return result;
      } catch (Throwable ex) {
        if (trx != null && trx.isActive()) {
          trx.rollback();
        }
        assert false;
        throw SneakyThrows.propagate(ex);
      }
      //ensureCompletion(session.getTransaction());
    } finally {
      ManagedSessionContext.unbind(sessionFactory);
    }
  }

  public void flush() {
    getSession().flush();
  }

  public @Nonnull E getReference(@Nonnull ID id) {
    SessionFactory sessionFactory = app.require(sessionFactoryKey);
    SessionProvider sessionProvider = app.require(sessionProviderKey);
    try (Session session = sessionProvider.newSession(sessionFactory.withOptions())) {
      ManagedSessionContext.bind(session);
      Transaction trx = null;
      try {
        trx = session.getTransaction();
        trx.begin();
        E result = _load(session, persistentClass, id);
        if (trx.isActive()) {
          trx.commit();
        }
        return result;
      } catch (Throwable ex) {
        if (trx != null && trx.isActive()) {
          trx.rollback();
        }
        assert false;
        throw SneakyThrows.propagate(ex);
      }
      //ensureCompletion(session.getTransaction());
    } finally {
      ManagedSessionContext.unbind(sessionFactory);
    }
  }

  public @Nonnull List<E> getReferences(@Nonnull Iterable<ID> ids) {
    SessionFactory sessionFactory = app.require(sessionFactoryKey);
    SessionProvider sessionProvider = app.require(sessionProviderKey);
    try (Session session = sessionProvider.newSession(sessionFactory.withOptions())) {
      ManagedSessionContext.bind(session);
      Transaction trx = null;
      try {
        trx = session.getTransaction();
        trx.begin();
        List<E> result = _load(session, persistentClass, ids);
        if (trx.isActive()) {
          trx.commit();
        }
        return result;
      } catch (Throwable ex) {
        if (trx != null && trx.isActive()) {
          trx.rollback();
        }
        assert false;
        throw SneakyThrows.propagate(ex);
      }
      //ensureCompletion(session.getTransaction());
    } finally {
      ManagedSessionContext.unbind(sessionFactory);
    }
  }

  public @Nonnull E[] getReferences(@Nonnull Serializable... ids) {
    SessionFactory sessionFactory = app.require(sessionFactoryKey);
    SessionProvider sessionProvider = app.require(sessionProviderKey);
    try (Session session = sessionProvider.newSession(sessionFactory.withOptions())) {
      ManagedSessionContext.bind(session);
      Transaction trx = null;
      try {
        trx = session.getTransaction();
        trx.begin();
        E[] result = _load(session, persistentClass, ids);
        if (trx.isActive()) {
          trx.commit();
        }
        return result;
      } catch (Throwable ex) {
        if (trx != null && trx.isActive()) {
          trx.rollback();
        }
        assert false;
        throw SneakyThrows.propagate(ex);
      }
      //ensureCompletion(session.getTransaction());
    } finally {
      ManagedSessionContext.unbind(sessionFactory);
    }
  }

  public @Nonnull E save(@Nonnull E entity) {
    SessionFactory sessionFactory = app.require(sessionFactoryKey);
    SessionProvider sessionProvider = app.require(sessionProviderKey);
    try (Session session = sessionProvider.newSession(sessionFactory.withOptions())) {
      ManagedSessionContext.bind(session);
      Transaction trx = null;
      try {
        trx = session.getTransaction();
        trx.begin();
        E result = _saveOrUpdateIsNew(session, entity);
        if (trx.isActive()) {
          trx.commit();
        }
        return result;
      } catch (Throwable ex) {
        if (trx != null && trx.isActive()) {
          trx.rollback();
        }
        assert false;
        throw SneakyThrows.propagate(ex);
      }
      //ensureCompletion(session.getTransaction());
    } finally {
      ManagedSessionContext.unbind(sessionFactory);
    }
  }

  public @Nonnull List<E> saveAll(@Nonnull Iterable<E> entities) {
    SessionFactory sessionFactory = app.require(sessionFactoryKey);
    SessionProvider sessionProvider = app.require(sessionProviderKey);
    try (Session session = sessionProvider.newSession(sessionFactory.withOptions())) {
      ManagedSessionContext.bind(session);
      Transaction trx = null;
      try {
        trx = session.getTransaction();
        trx.begin();
        List<E> result = _saveOrUpdateIsNew(session, entities);
        if (trx.isActive()) {
          trx.commit();
        }
        return result;
      } catch (Throwable ex) {
        if (trx != null && trx.isActive()) {
          trx.rollback();
        }
        assert false;
        throw SneakyThrows.propagate(ex);
      }
      //ensureCompletion(session.getTransaction());
    } finally {
      ManagedSessionContext.unbind(sessionFactory);
    }
  }

  public @Nonnull E update(@Nonnull E entity) {
    SessionFactory sessionFactory = app.require(sessionFactoryKey);
    SessionProvider sessionProvider = app.require(sessionProviderKey);
    try (Session session = sessionProvider.newSession(sessionFactory.withOptions())) {
      ManagedSessionContext.bind(session);
      Transaction trx = null;
      try {
        trx = session.getTransaction();
        trx.begin();
        E result = _saveOrUpdateIsNew(session, entity);
        if (trx.isActive()) {
          trx.commit();
        }
        return result;
      } catch (Throwable ex) {
        if (trx != null && trx.isActive()) {
          trx.rollback();
        }
        assert false;
        throw SneakyThrows.propagate(ex);
      }
      //ensureCompletion(session.getTransaction());
    } finally {
      ManagedSessionContext.unbind(sessionFactory);
    }
  }

  public @Nonnull List<E> updateAll(@Nonnull Iterable<E> entities) {
    SessionFactory sessionFactory = app.require(sessionFactoryKey);
    SessionProvider sessionProvider = app.require(sessionProviderKey);
    try (Session session = sessionProvider.newSession(sessionFactory.withOptions())) {
      ManagedSessionContext.bind(session);
      Transaction trx = null;
      try {
        trx = session.getTransaction();
        trx.begin();
        List<E> result = _saveOrUpdateIsNew(session, entities);
        if (trx.isActive()) {
          trx.commit();
        }
        return result;
      } catch (Throwable ex) {
        if (trx != null && trx.isActive()) {
          trx.rollback();
        }
        assert false;
        throw SneakyThrows.propagate(ex);
      }
      //ensureCompletion(session.getTransaction());
    } finally {
      ManagedSessionContext.unbind(sessionFactory);
    }
  }

  public long count() {
    SessionFactory sessionFactory = app.require(sessionFactoryKey);
    SessionProvider sessionProvider = app.require(sessionProviderKey);
    try (Session session = sessionProvider.newSession(sessionFactory.withOptions())) {
      ManagedSessionContext.bind(session);
      Transaction trx = null;
      try {
        trx = session.getTransaction();
        trx.begin();
        long result = _count(session, persistentClass);
        if (trx.isActive()) {
          trx.commit();
        }
        return result;
      } catch (Throwable ex) {
        if (trx != null && trx.isActive()) {
          trx.rollback();
        }
        assert false;
        throw SneakyThrows.propagate(ex);
      }
      //ensureCompletion(session.getTransaction());
    } finally {
      ManagedSessionContext.unbind(sessionFactory);
    }
  }

  public long count(ImmutableSearch search) {
    if (search == null) {
      search = new Search();
    }
    SessionFactory sessionFactory = app.require(sessionFactoryKey);
    SessionProvider sessionProvider = app.require(sessionProviderKey);
    try (Session session = sessionProvider.newSession(sessionFactory.withOptions())) {
      ManagedSessionContext.bind(session);
      Transaction trx = null;
      try {
        trx = session.getTransaction();
        trx.begin();
        long result = _count(session, persistentClass, search);
        if (trx.isActive()) {
          trx.commit();
        }
        return result;
      } catch (Throwable ex) {
        if (trx != null && trx.isActive()) {
          trx.rollback();
        }
        assert false;
        throw SneakyThrows.propagate(ex);
      }
      //ensureCompletion(session.getTransaction());
    } finally {
      ManagedSessionContext.unbind(sessionFactory);
    }
  }

  @SuppressWarnings("unchecked")
  public @Nonnull <RT> List<RT> search(ImmutableSearch search) {
    if (search == null) {
      return (List<RT>) findAll();
    }
    SessionFactory sessionFactory = app.require(sessionFactoryKey);
    SessionProvider sessionProvider = app.require(sessionProviderKey);
    try (Session session = sessionProvider.newSession(sessionFactory.withOptions())) {
      ManagedSessionContext.bind(session);
      Transaction trx = null;
      try {
        trx = session.getTransaction();
        trx.begin();
        List<RT> result = _search(session, persistentClass, search);
        if (trx.isActive()) {
          trx.commit();
        }
        return result;
      } catch (Throwable ex) {
        if (trx != null && trx.isActive()) {
          trx.rollback();
        }
        assert false;
        throw SneakyThrows.propagate(ex);
      }
      //ensureCompletion(session.getTransaction());
    } finally {
      ManagedSessionContext.unbind(sessionFactory);
    }
  }

  @SuppressWarnings("unchecked")
  public <RT> SearchResult<RT> searchAndCount(ImmutableSearch search) {
    if (search == null) {
      SearchResult<RT> result = new SearchResult<>();
      result.setResult((List<RT>) findAll());
      result.setTotalCount(result.getResult().size());
      return result;
    }
    SessionFactory sessionFactory = app.require(sessionFactoryKey);
    SessionProvider sessionProvider = app.require(sessionProviderKey);
    try (Session session = sessionProvider.newSession(sessionFactory.withOptions())) {
      ManagedSessionContext.bind(session);
      Transaction trx = null;
      try {
        trx = session.getTransaction();
        trx.begin();
        SearchResult<RT> result = _searchAndCount(session, persistentClass, search);
        if (trx.isActive()) {
          trx.commit();
        }
        return result;
      } catch (Throwable ex) {
        if (trx != null && trx.isActive()) {
          trx.rollback();
        }
        assert false;
        throw SneakyThrows.propagate(ex);
      }
      //ensureCompletion(session.getTransaction());
    } finally {
      ManagedSessionContext.unbind(sessionFactory);
    }
  }

  @SuppressWarnings("unchecked")
  public @Nullable <RT> RT searchUnique(ImmutableSearch search) {
    SessionFactory sessionFactory = app.require(sessionFactoryKey);
    SessionProvider sessionProvider = app.require(sessionProviderKey);
    try (Session session = sessionProvider.newSession(sessionFactory.withOptions())) {
      ManagedSessionContext.bind(session);
      Transaction trx = null;
      try {
        trx = session.getTransaction();
        trx.begin();
        RT result = (RT) _searchUnique(session, persistentClass, search);
        if (trx.isActive()) {
          trx.commit();
        }
        return result;
      } catch (Throwable ex) {
        if (trx != null && trx.isActive()) {
          trx.rollback();
        }
        assert false;
        throw SneakyThrows.propagate(ex);
      }
      //ensureCompletion(session.getTransaction());
    } finally {
      ManagedSessionContext.unbind(sessionFactory);
    }
  }

  // Private methods

  private void _delete(@Nonnull Session session, Object entity) {
    if (entity != null) {
      Serializable id = getMetadataUtils().getId(entity);
      if (id != null) {
        entity = session.get(getMetadataUtils().getUnproxiedClass(entity), id);
        if (entity != null) {
          session.delete(entity);
        }
      }
    }
  }

  private void _delete(@Nonnull Session session, @Nonnull Iterable<? extends E> entities) {
    for (Object entity : entities) {
      if (entity != null) {
        session.delete(entity);
      }
    }
  }

  private void _deleteById(@Nonnull Session session, Class<?> type, Serializable id) {
    if (id != null) {
      type = getMetadataUtils().getUnproxiedClass(type);
      Object entity = session.get(type, id);
      if (entity != null) {
        session.delete(entity);
      }
    }
  }

  private void _deleteById(@Nonnull Session session, Class<?> type, @Nonnull Iterable<? extends ID> ids) {
    type = getMetadataUtils().getUnproxiedClass(type);
    /*
     * Criteria c = session.createCriteria(type);
     * c.add(Restrictions.in("id", ids));
     * for (Object entity : c.list()) {
     *     session.delete(entity);
     * }
     */
    CriteriaBuilder cb = session.getCriteriaBuilder();
    CriteriaQuery<?> query = cb.createQuery(type);
    Root<?> root = query.from(type);
    query.where(cb.equal(root.get("id"), Restrictions.in("id", ids)));
    List<?> list = session.createQuery(query).getResultList();
    for (Object entity : list) {
      session.delete(entity);
    }
  }

  private boolean _exists(@Nonnull Session session, Object entity) {
    if (session.contains(entity)) {
      return true;
    }

    return _existsById(session, entity.getClass(), getMetadataUtils().getId(entity));
  }

  private boolean _existsById(Session session, Class<?> type, Serializable id) {
    if (type == null) {
      throw new NullPointerException("Type is null.");
    }

    if (id == null) {
      return false;
    }

    type = getMetadataUtils().getUnproxiedClass(type);

    return session
        .createQuery("select e.id from " + getMetadataUtils().get(type).getEntityName() + " e where e.id = :id")
        .setParameter("id", id)
        .getResultList().size() == 1;
  }

  private List<E> _all(@Nonnull Session session, Class<E> type) {
    type = getMetadataUtils().getUnproxiedClass(type);
    return session
        .createQuery("select e from " + type.getSimpleName() + " e order by e.id", type)
        .getResultList();
  }

  private E _get(@Nonnull Session session, Class<E> type, Serializable id) {
    type = getMetadataUtils().getUnproxiedClass(type);
    return session.get(type, id);
  }

  private List<E> _get(@Nonnull Session session, Class<E> type, @Nonnull Iterable<ID> ids) {
    type = getMetadataUtils().getUnproxiedClass(type);
    return session
        .createQuery("select e from " + type.getSimpleName() + " e where e.id in :ids", type)
        .setParameter("ids", ids)
        .getResultList();
  }

  /**
   * Returns the persistent instance of the given entity class with the given identifier,
   * assuming that the instance exists. Throw an unrecoverable exception if there is no
   * matching database row. If the class is mapped with a proxy, {@code load()} just returns
   * an uninitialized proxy and does not actually hit the database until you invoke a method
   * of the proxy. This behaviour is very useful if you wish to create an association to an
   * object without actually loading it from the database. It also allows multiple instances
   * to be loaded as a batch if batch-size is defined for the class mapping.
   */
  private E _load(@Nonnull Session session, Class<E> type, Serializable id) {
    type = getMetadataUtils().getUnproxiedClass(type); //Get the real entity class
    return session.load(type, id);
  }

  /**
   * Returns the persistent instance of the given entity class with the given identifier,
   * assuming that the instance exists. Throw an unrecoverable exception if there is no
   * matching database row. An array of entities is returned that matches the same order
   * of the ids listed in the call. For each entity that is not found in the datastore,
   * a null will be inserted in its place in the return array.
   *
   * @see #_load(Session, Class, Serializable)
   */
  private @Nonnull List<E> _load(@Nonnull Session session, Class<E> type, @Nonnull Iterable<ID> ids) {
    type = getMetadataUtils().getUnproxiedClass(type); //Get the real entity class
    List<E> retVal = new ArrayList<>();
    for (ID id : ids) {
      retVal.add(_load(session, type, id));
    }
    return retVal;
  }

  /**
   * Returns the persistent instance of the given entity class with the given identifier,
   * assuming that the instance exists. Throw an unrecoverable exception if there is no
   * matching database row. An array of entities is returned that matches the same order
   * of the ids listed in the call. For each entity that is not found in the datastore,
   * a null will be inserted in its place in the return array.
   *
   * @see #_load(Session, Class, Serializable)
   */
  @SuppressWarnings("unchecked")
  private E[] _load(Session session, Class<E> type, @Nonnull Serializable... ids) {
    type = getMetadataUtils().getUnproxiedClass(type); //Get the real entity class
    Object[] retVal = (Object[]) Array.newInstance(type, ids.length);
    for (int i = 0; i < ids.length; i++) {
      if (ids[i] != null)
        retVal[i] = _load(session, type, ids[i]);
    }
    return (E[]) retVal;
  }

  /**
   * Read the persistent state associated with the given identifier into the given transient
   * instance. Throw an unrecoverable exception if there is no matching database row.
   */
  private void _load(@Nonnull Session session, Object transientEntity, Serializable id) {
    session.load(transientEntity, id);
  }

  private Serializable _save(@Nonnull Session session, E entity) {
    return session.save(entity);
  }

  private E _saveOrUpdateIsNew(@Nonnull Session session, E entity) {
    if (entity == null)
      throw new IllegalArgumentException("Attempt to saveOrUpdate with null entity!");

    Serializable id = getMetadataUtils().getId(entity);

    if (id == null || (Long.valueOf(0)).equals(id) || !_exists(session, entity)) {
      Serializable persistentEntity = _save(session, entity);
      return _get(session, persistentClass, persistentEntity);
    } else {
      _update(session, entity);
      return _get(session, persistentClass, getMetadataUtils().getId(entity));
    }
  }

  private @Nonnull List<E> _saveOrUpdateIsNew(@Nonnull Session session, @Nonnull Iterable<E> entities) {

    List<E> persistenceEntities = new ArrayList<>();

    for (E entity : entities) {
      E persistenceEntity = _saveOrUpdateIsNew(session, entity);
      persistenceEntities.add(persistenceEntity);
    }

    return persistenceEntities;
  }

  private void _saveOrUpdate(@Nonnull Session session, E entity) {
    session.saveOrUpdate(entity);
  }

  private void _update(@Nonnull Session session, E entity) {
    session.update(entity);
  }

  /**
   * Retrieves the total number of results that would be returned using the given
   * {@link ImmutableSearch} if there were no paging or maxResult limits.
   */
  private long _count(@Nonnull Session session, ImmutableSearch search) {
    if (search == null)
      throw new NullPointerException("Search is null.");
    if (search.getSearchClass() == null)
      throw new NullPointerException("Search class is null.");

    return getSearchProcessor().count(session, search);
  }

  /**
   * Same as {@code _count(ImmutableSearch)} except that it uses the specified search class
   * instead of getting it from the search object. Also, if the search object has a different
   * search class than what is specified, an exception is thrown.
   */
  private long _count(@Nonnull Session session, Class<?> searchClass, ImmutableSearch search) {
    if (search == null)
      throw new NullPointerException("Search is null.");
    if (searchClass == null)
      throw new NullPointerException("Search class is null.");
    if (search.getSearchClass() != null && !search.getSearchClass().equals(searchClass))
      throw new IllegalArgumentException("Search class does not match expected type: " + searchClass.getName());

    return getSearchProcessor().count(session, searchClass, search);
  }

  /**
   * Retrieves the total number of instances of this class.
   */
  private long _count(@Nonnull Session session, Class<?> type) {
    List<?> counts = session
        .createQuery("select count(_it_) from " + getMetadataUtils().get(type).getEntityName() + " _it_")
        .getResultList();
    int sum = 0;

    for (Object count : counts) {
      sum += ((long) count);
    }

    return sum;
  }

  /**
   * Search for objects based on the search parameters in the specified {@link ImmutableSearch}
   * object.
   */
  private List<?> _search(@Nonnull Session session, ImmutableSearch search) {
    if (search == null)
      throw new NullPointerException("Search is null.");
    if (search.getSearchClass() == null)
      throw new NullPointerException("Search class is null.");

    return getSearchProcessor().search(session, search);
  }

  /**
   * Same as {@code _search(ImmutableSearch)} except that it uses the specified search class
   * instead of getting it from the search object. Also, if the search object has a different
   * search class than what is specified, an exception is thrown.
   */
  private <RT> List<RT> _search(@Nonnull Session session, Class<?> searchClass, ImmutableSearch search) {
    if (search == null)
      throw new NullPointerException("Search is null.");
    if (searchClass == null)
      throw new NullPointerException("Search class is null.");
    if (search.getSearchClass() != null && !search.getSearchClass().equals(searchClass))
      throw new IllegalArgumentException("Search class does not match expected type: " + searchClass.getName());

    return getSearchProcessor().search(session, searchClass, search);
  }

  /**
   * Returns a {@link SearchResult} object that includes the list of results like
   * {@code search()} and the total length like {@literal searchLength}.
   */
  private <RT> SearchResult<RT> _searchAndCount(@Nonnull Session session, ImmutableSearch search) {
    if (search == null)
      throw new NullPointerException("Search is null.");
    if (search.getSearchClass() == null)
      throw new NullPointerException("Search class is null.");

    return getSearchProcessor().searchAndCount(session, search);
  }

  /**
   * Same as {@code _searchAndCount(ImmutableSearch)} except that it uses the specified search
   * class instead of getting it from the search object. Also, if the search object has a
   * different search class than what is specified an exception is thrown.
   */
  private <RT> SearchResult<RT> _searchAndCount(@Nonnull Session session, Class<?> searchClass, ImmutableSearch search) {
    if (search == null)
      throw new NullPointerException("Search is null.");
    if (searchClass == null)
      throw new NullPointerException("Search class is null.");
    if (search.getSearchClass() != null && !search.getSearchClass().equals(searchClass))
      throw new IllegalArgumentException("Search class does not match expected type: " + searchClass.getName());

    return getSearchProcessor().searchAndCount(session, searchClass, search);
  }

  /**
   * Search for a single result using the given parameters.
   */
  private Object _searchUnique(@Nonnull Session session, ImmutableSearch search) throws NonUniqueResultException {
    if (search == null)
      throw new NullPointerException("Search is null.");
    if (search.getSearchClass() == null)
      throw new NullPointerException("Search class is null.");

    return getSearchProcessor().searchUnique(session, search);
  }

  /**
   * Same as {@code _searchUnique(ImmutableSearch)} except that it uses the specified search
   * class instead of getting it from the search object. Also, if the search object has a
   * different search class than what is specified, an exception is thrown.
   */
  private Object _searchUnique(@Nonnull Session session, Class<?> searchClass, ImmutableSearch search) {
    if (search == null)
      throw new NullPointerException("Search is null.");
    if (searchClass == null)
      throw new NullPointerException("Search class is null.");
    if (search.getSearchClass() != null && !search.getSearchClass().equals(searchClass))
      throw new IllegalArgumentException("Search class does not match expected type: " + searchClass.getName());

    return getSearchProcessor().searchUnique(session, searchClass, search);
  }

}
