package io.xammax.jooby.hibernate.search;

import java.util.Locale;

public enum FieldOperator {

  /**
   * Possible value for {@code operator}. This is the default value and does not apply any
   * operator to the column. All rows in the result set are returned.
   */
  OP_PROPERTY,

  /**
   * Possible value for {@code operator}. Returns the number of rows in the result set where
   * the given property is non-null.
   */
  OP_COUNT,

  /**
   * Possible value for {@code operator}. Returns the number of distinct values of the given
   * property in the result set.
   */
  OP_COUNT_DISTINCT,

  /**
   * Possible value for {@code operator}. Returns the maximum value of the given property in
   * the result set.
   */
  OP_MAX,

  /**
   * Possible value for {@code operator}. Returns the minimum value of the given property in
   * the result set.
   */
  OP_MIN,

  /**
   * Possible value for {@code operator}. Returns the sum of the given property in all rows of
   * the result set.
   */
  OP_SUM,

  /**
   * Possible value for {@code operator}. Returns the average value of the given property in
   * the result set.
   */
  OP_AVG,

  /**
   * Possible value for {@code operator}. Allows a free-form JPQL/HQL select-clause expression.
   * The expression is set in the "property" property of the {@link Field}. Reference
   * properties by wrapping them with curly braces ({}). For examples:
   *
   * {@code
   *     new Field("{firstName}||' '||{initial}||' '||upper({lastName})", Field.OP_CUSTOM);
   *     new Field("max(({top} - {bottom}) / 2)", Field.OP_CUSTOM);
   * }
   */
  OP_CUSTOM;

  /**
   * Returns the {@link FieldOperator} enum for the given {@link String} value.
   *
   * @param value from {@link FieldOperator} enum, not require uppercase
   * @return {@link FieldOperator} enum to upperCase
   * @throws IllegalArgumentException in case the given value cannot be parsed into an enum value
   */
  public static FieldOperator fromString(String value) {
    try {
      return FieldOperator.valueOf(value.toUpperCase(Locale.US));
    } catch (Exception e) {
      throw new IllegalArgumentException(String.format("Invalid value '%s' for fields given!", value), e);
    }
  }

}
