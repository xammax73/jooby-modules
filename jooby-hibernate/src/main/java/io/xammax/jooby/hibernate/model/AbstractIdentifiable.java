package io.xammax.jooby.hibernate.model;

import javax.persistence.MappedSuperclass;
import java.io.Serializable;

/**
 * Abstract base class for dtos. Allows parameterization of id type and implements
 * {@code #equals({@link Object})} and {@code #hashCode()} based on that id.
 *
 * @param <ID> the type of the dto identifier
 */
@MappedSuperclass
public abstract class AbstractIdentifiable<ID extends Serializable> implements Identifiable<ID> {

  public abstract ID getId();

  @Override
  public boolean equals(Object obj) {
    if (null == obj) {
      return false;
    }
    if (this == obj) {
      return true;
    }
    if (!(obj instanceof AbstractIdentifiable)) {
      return false;
    }
    if (getId() == null || ((AbstractIdentifiable<?>) obj).getId() == null) {
      return false;
    }
    return getId().equals(((AbstractIdentifiable<?>) obj).getId());
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
    return result;
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() +
        (getId() != null ? ", id = " + getId() : "");
  }

}
