package io.xammax.jooby.hibernate.search;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * A fully-featured implementation of {@link ImmutableSearch} and {@link MutableSearch}.
 */
public class Search implements MutableSearch, Serializable {

  private static final long serialVersionUID = 1L;

  protected int firstResult = -1; // -1 stands for unspecified

  protected int maxResults = -1; // -1 stands for unspecified

  protected int pageNumber = -1; // -1 stands for unspecified

  protected Class<?> searchClass;

  protected List<String> fetches = new ArrayList<>();

  protected List<Filter> filters = new ArrayList<>();

  protected List<Sort> sorts = new ArrayList<>();

  protected List<Field> fields = new ArrayList<>();

  protected List<String> joins = new ArrayList<>();

  protected boolean disjunction;

  protected boolean distinct;

  protected Result resultMode = Result.RESULT_AUTO;

  protected Class<?> resultMapClass;

  public Search() { }

  public Search(Class<?> searchClass) {
    this.searchClass = searchClass;
  }

  public Class<?> getSearchClass() {
    return searchClass;
  }

  public Search setSearchClass(Class<?> searchClass) {
    this.searchClass = searchClass;
    return this;
  }

  /* Fetches */

  public Search addFetch(String property) {
    SearchUtils.addFetch(this, property);
    return this;
  }

  public Search addFetches(String... properties) {
    SearchUtils.addFetches(this, properties);
    return this;
  }

  public void removeFetch(String property) {
    SearchUtils.removeFetch(this, property);
  }

  public void clearFetches() {
    SearchUtils.clearFetches(this);
  }

  /* Fields */

  public Search addField(Field field) {
    SearchUtils.addField(this, field);
    return this;
  }

  public Search addFields(Field... fields) {
    SearchUtils.addFields(this, fields);
    return this;
  }

  /**
   * If field is used with {@code resultMode == RESULT_MAP}, the {@literal property} will also
   * be used as the key for this value in the map.
   */
  public Search addField(String property) {
    SearchUtils.addField(this, property);
    return this;
  }

  /**
   * If field is used with {@code resultMode == RESULT_MAP}, the {@literal key} will also be
   * used as the key for this value in the map.
   */
  public Search addField(String property, String key) {
    SearchUtils.addField(this, property, key);
    return this;
  }

  /**
   * If field is used with {@code resultMode == RESULT_MAP}, the {@literal property} will also
   * be used as the key for this value in the map.
   */
  public Search addField(String property, FieldOperator operator) {
    SearchUtils.addField(this, property, operator);
    return this;
  }

  /**
   * If field is used with {@code resultMode == RESULT_MAP}, the {@literal key} will also be
   * used as the key for this value in the map.
   */
  public Search addField(String property, String key, FieldOperator operator) {
    SearchUtils.addField(this, property, key, operator);
    return this;
  }

  public void removeField(Field field) {
    SearchUtils.removeField(this, field);
  }

  public void removeField(String property) {
    SearchUtils.removeField(this, property);
  }

  public void removeField(String property, String key) {
    SearchUtils.removeField(this, property, key);
  }

  public void clearFields() {
    SearchUtils.clearFields(this);
  }

  /* Filters */

  public Search addFilter(Filter filter) {
    SearchUtils.addFilter(this, filter);
    return this;
  }

  public Search addFilters(Filter... filters) {
    SearchUtils.addFilters(this, filters);
    return this;
  }

  /**
   * Add a filter that uses the {@literal ==} operator.
   */
  public Search addFilterEqual(String property, Object value) {
    SearchUtils.addFilterEqual(this, property, value);
    return this;
  }

  /**
   * Add a filter that uses the {@literal >=} operator.
   */
  public Search addFilterGreaterOrEqual(String property, Object value) {
    SearchUtils.addFilterGreaterOrEqual(this, property, value);
    return this;
  }

  /**
   * Add a filter that uses the {@literal >} operator.
   */
  public Search addFilterGreaterThan(String property, Object value) {
    SearchUtils.addFilterGreaterThan(this, property, value);
    return this;
  }

  /**
   * Add a filter that uses the {@literal IN} operator.
   */
  public Search addFilterIn(String property, Collection<?> value) {
    SearchUtils.addFilterIn(this, property, value);
    return this;
  }

  /**
   * Add a filter that uses the {@literal IN} operator.
   */
  public Search addFilterIn(String property, Object... value) {
    SearchUtils.addFilterIn(this, property, value);
    return this;
  }

  /**
   * Add a filter that uses the {@literal NOT IN} operator.
   */
  public Search addFilterNotIn(String property, Collection<?> value) {
    SearchUtils.addFilterNotIn(this, property, value);
    return this;
  }

  /**
   * Add a filter that uses the {@literal NOT IN} operator.
   */
  public Search addFilterNotIn(String property, Object... value) {
    SearchUtils.addFilterNotIn(this, property, value);
    return this;
  }

  /**
   * Add a filter that uses the {@literal <=} operator.
   */
  public Search addFilterLessOrEqual(String property, Object value) {
    SearchUtils.addFilterLessOrEqual(this, property, value);
    return this;
  }

  /**
   * Add a filter that uses the {@literal <} operator.
   */
  public Search addFilterLessThan(String property, Object value) {
    SearchUtils.addFilterLessThan(this, property, value);
    return this;
  }

  /**
   * Add a filter that uses the {@literal LIKE} operator.
   */
  public Search addFilterLike(String property, String value) {
    SearchUtils.addFilterLike(this, property, value);
    return this;
  }

  /**
   * Add a filter that uses the {@literal ILIKE} operator.
   */
  public Search addFilterILike(String property, String value) {
    SearchUtils.addFilterILike(this, property, value);
    return this;
  }

  /**
   * Add a filter that uses the {@literal !=} operator.
   */
  public Search addFilterNotEqual(String property, Object value) {
    SearchUtils.addFilterNotEqual(this, property, value);
    return this;
  }

  /**
   * Add a filter that uses the {@literal IS NULL} operator.
   */
  public Search addFilterNull(String property) {
    SearchUtils.addFilterNull(this, property);
    return this;
  }

  /**
   * Add a filter that uses the {@literal IS NOT NULL} operator.
   */
  public Search addFilterNotNull(String property) {
    SearchUtils.addFilterNotNull(this, property);
    return this;
  }

  /**
   * Add a filter that uses the {@literal IS EMPTY} operator.
   */
  public Search addFilterEmpty(String property) {
    SearchUtils.addFilterEmpty(this, property);
    return this;
  }

  /**
   * Add a filter that uses the {@literal IS NOT EMPTY} operator.
   */
  public Search addFilterNotEmpty(String property) {
    SearchUtils.addFilterNotEmpty(this, property);
    return this;
  }

  /**
   * Add a filter that uses the {@literal AND} operator.
   */
  public Search addFilterAnd(Filter... filters) {
    SearchUtils.addFilterAnd(this, filters);
    return this;
  }

  /**
   * Add a filter that uses the {@literal OR} operator.
   */
  public Search addFilterOr(Filter... filters) {
    SearchUtils.addFilterOr(this, filters);
    return this;
  }

  /**
   * Add a filter that uses the {@literal NOT} operator.
   */
  public Search addFilterNot(Filter filter) {
    SearchUtils.addFilterNot(this, filter);
    return this;
  }

  /**
   * Add a filter that uses the {@literal SOME} operator.
   */
  public Search addFilterSome(String property, Filter filter) {
    SearchUtils.addFilterSome(this, property, filter);
    return this;
  }

  /**
   * Add a filter that uses the {@literal ALL} operator.
   */
  public Search addFilterAll(String property, Filter filter) {
    SearchUtils.addFilterAll(this, property, filter);
    return this;
  }

  /**
   * Add a filter that uses the {@literal NONE} operator.
   */
  public Search addFilterNone(String property, Filter filter) {
    SearchUtils.addFilterNone(this, property, filter);
    return this;
  }

  public void removeFilter(Filter filter) {
    SearchUtils.removeFilter(this, filter);
  }

  /**
   * Remove all filters on the given property.
   */
  public void removeFiltersOnProperty(String property) {
    SearchUtils.removeFiltersOnProperty(this, property);
  }

  public void clearFilters() {
    SearchUtils.clearFilters(this);
  }

  /* Paging */

  public int getFirstResult() {
    return firstResult;
  }

  public Search setFirstResult(int firstResult) {
    this.firstResult = firstResult;
    return this;
  }

  public int getMaxResults() {
    return maxResults;
  }

  public Search setMaxResults(int maxResults) {
    this.maxResults = maxResults;
    return this;
  }

  public int getPageNumber() {
    return pageNumber;
  }

  public Search setPageNumber(int pageNumber) {
    this.pageNumber = pageNumber;
    return this;
  }

  public void clearPaging() {
    SearchUtils.clearPaging(this);
  }

  /* Sorts */

  public Search addSort(Sort sort) {
    SearchUtils.addSort(this, sort);
    return this;
  }

  /**
   * Add sort by property. {@literal direction} can be {@literal null}.
   * Ascending if {@code direction == ASC}, descending if {@code direction == DESC}.
   */
  public Search addSort(String property, Direction direction) {
    SearchUtils.addSort(this, property, direction);
    return this;
  }

  /**
   * Add sort by property. {@literal direction} can be {@literal null}.
   * Ascending if {@code direction == ASC}, descending if {@code direction == DESC}.
   */
  public Search addSort(String property, Direction direction, boolean ignoreCase) {
    SearchUtils.addSort(this, property, direction, ignoreCase);
    return this;
  }

  public Search addSorts(Sort... sorts) {
    SearchUtils.addSorts(this, sorts);
    return this;
  }

  /**
   * Add ascending sort by property.
   */
  public Search addSortAsc(String property) {
    SearchUtils.addSortAsc(this, property);
    return this;
  }

  /**
   * Add ascending sort by property.
   */
  public Search addSortAsc(String property, boolean ignoreCase) {
    SearchUtils.addSortAsc(this, property, ignoreCase);
    return this;
  }

  /**
   * Add descending sort by property.
   */
  public Search addSortDesc(String property) {
    SearchUtils.addSortDesc(this, property);
    return this;
  }

  /**
   * Add descending sort by property.
   */
  public Search addSortDesc(String property, boolean ignoreCase) {
    SearchUtils.addSortDesc(this, property, ignoreCase);
    return this;
  }

  public void removeSort(Sort sort) {
    SearchUtils.removeSort(this, sort);
  }

  public void removeSort(String property) {
    SearchUtils.removeSort(this, property);
  }

  public void clearSorts() {
    SearchUtils.clearSorts(this);
  }

  /* All */

  public void clear() {
    SearchUtils.clear(this);
  }

  public List<String> getFetches() {
    return fetches;
  }

  public Search setFetches(List<String> fetches) {
    this.fetches = fetches;
    return this;
  }

  public List<Field> getFields() {
    return fields;
  }

  public Search setFields(List<Field> fields) {
    this.fields = fields;
    return this;
  }

  public List<Filter> getFilters() {
    return filters;
  }

  public Search setFilters(List<Filter> filters) {
    this.filters = filters;
    return this;
  }

  public List<Sort> getSorts() {
    return sorts;
  }

  public Search setSorts(List<Sort> sorts) {
    this.sorts = sorts;
    return this;
  }

  public boolean isDisjunction() {
    return disjunction;
  }

  /**
   * Filters added to a search are "ANDed" together if this is {@code disjunction=false} and
   * "ORed" if it is set to true.
   */
  public Search setDisjunction(boolean disjunction) {
    this.disjunction = disjunction;
    return this;
  }

  public boolean isDistinct() {
    return distinct;
  }

  public Search setDistinct(boolean distinct) {
    this.distinct = distinct;
    return this;
  }

  public Result getResultMode() {
    return resultMode;
  }

  /**
   * @param resultMode any of
   *        {@link Result#RESULT_AUTO},
   *        {@link Result#RESULT_ARRAY},
   *        {@link Result#RESULT_LIST},
   *        {@link Result#RESULT_MAP},
   *        {@link Result#RESULT_SINGLE}.
   */
  public Search setResultMode(Result resultMode) {
    if (resultMode != Result.RESULT_AUTO &&
        resultMode != Result.RESULT_ARRAY &&
        resultMode != Result.RESULT_LIST &&
        resultMode != Result.RESULT_MAP &&
        resultMode != Result.RESULT_SINGLE)
      throw new IllegalArgumentException("Result Mode ( " + resultMode + " ) is not a valid option.");
    this.resultMode = resultMode;
    return this;
  }

  public Class<?> getResultMapClass() {
    return resultMapClass;
  }

  /**
   * Maps result to clazz
   *
   * @param cls class
   * @return current search
   */
  public Search setResultMapClass(Class<?> cls) {
    this.resultMapClass = cls;
    return this;
  }

  public List<String> getJoins() {
    return joins;
  }

  /**
   * @param joins list of joins.
   * @return this with list {@code joins} param.
   */
  public Search setJoins(List<String> joins) {
    this.joins = joins;
    return this;
  }

}

