package io.xammax.jooby.hibernate.search;

import java.util.List;

public interface MutableSearch extends ImmutableSearch {

  /**
   * Sets index of first result record.
   *
   * @param firstResult number of first record
   * @return this with {@literal firstResult}
   */
  MutableSearch setFirstResult(int firstResult);

  /**
   * Sets maximum number of records on page.
   *
   * @param maxResults number of records
   * @return this with {@literal maxResults}
   */
  MutableSearch setMaxResults(int maxResults);

  /**
   * Sets number of the page of records.
   *
   * @param pageNumber number of the page of records
   * @return this with {@literal pageNumber}
   */
  MutableSearch setPageNumber(int pageNumber);

  /**
   * Sets search query type class.
   *
   * @param searchClass search query type class
   * @return this with {@literal searchClass}
   */
  MutableSearch setSearchClass(Class<?> searchClass);

  /**
   * Sets list of {@link String} fetches.
   *
   * @param fetches list of {@link String}
   * @return this with {@literal fetches} list
   */
  MutableSearch setFetches(List<String> fetches);

  /**
   * Sets list of {@link Field} fields.
   *
   * @param fields list of {@link Field}
   * @return this with {@literal fields} list
   */
  MutableSearch setFields(List<Field> fields);

  /**
   * Sets list of {@link Filter} filters.
   *
   * @param filters list of {@link Filter}
   * @return this with {@literal filters} list
   */
  MutableSearch setFilters(List<Filter> filters);

  /**
   * Sets list of {@link Sort} sorts.
   *
   * @param sorts list of {@link Sort}
   * @return this with {@literal sorts} list
   */
  MutableSearch setSorts(List<Sort> sorts);

  /**
   * Sets {@literal disjunction} value.
   *
   * @param disjunction boolean value
   * @return this with {@literal disjunction} value
   */
  MutableSearch setDisjunction(boolean disjunction);

  /**
   * Sets {@literal distinct} value.
   *
   * @param distinct boolean value
   * @return this with {@literal distinct} value
   */
  MutableSearch setDistinct(boolean distinct);

  /**
   * Sets all of {@literal joins} of list.
   *
   * @param joins list of joins.
   * @return this with list {@code joins} param.
   */
  MutableSearch setJoins(List<String> joins);

  /**
   * Sets {@literal resultMode} of {@link Result}.
   *
   * @param resultMode {@link Result} mode
   * @return this
   */
  MutableSearch setResultMode(Result resultMode);

  /**
   * Sets {@literal cls} map type class.
   *
   * @param cls map type class
   * @return this with {@literal cls} param.
   */
  MutableSearch setResultMapClass(Class<?> cls);

}
