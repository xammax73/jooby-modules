package io.xammax.jooby.hibernate.extension.wrapper;

import io.xammax.jooby.hibernate.model.Persistable;
import org.hibernate.metadata.ClassMetadata;

import javax.annotation.Nullable;
import java.io.Serializable;
import java.util.List;

public interface EntityWrapper<E extends Persistable<ID>, ID extends Serializable> {

  /**
   * Get the currently wrapped entity.
   *
   * @return the entity
   */
  @Nullable E getObject();

  /**
   * Returns a property's value from the wrapped object.
   *
   * @param property name
   * @return property's value
   */
  @Nullable Object getPropertyValue(String property);

  /**
   * Sets a property's value on the wrapped object.
   *
   * @param property property name
   * @param value property value
   */
  EntityWrapper<E, ID> setPropertyValue(String property, Object value);

  /**
   * Checks if the identifier is valid.
   *
   * @return {@literal true} if an entity identifier is valid, {@literal false} otherwise
   */
  boolean hasValidIdentifier();

  /**
   * Gets the object metadata.
   *
   * @return ClassMetadata
   */
  ClassMetadata getMetadata();

  /**
   * Get the {@literal list} of object identifiers, single or composite.
   *
   * @param single property for choice
   * @return {@literal list} of multiple {@literal id} if a composite value, otherwise a single
   */
  List<ID> getIdentifier(@Nullable Boolean single);




}
