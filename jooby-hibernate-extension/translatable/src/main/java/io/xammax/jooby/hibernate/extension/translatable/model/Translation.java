package io.xammax.jooby.hibernate.extension.translatable.model;

import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity(name = "Translation")
@Table(
    name = "ext_translations",
    indexes = {
        @Index(name = "translation_idx", columnList = "language, object_class, foreign_key"),
        @Index(name = "general_translation_idx", columnList = "object_class, foreign_key")
    },
    uniqueConstraints = {
        @UniqueConstraint(name = "uk_translation_idx", columnNames = { "language", "object_class", "field", "foreign_key" })
    }
)
public class Translation extends AbstractTranslation { }
