package io.xammax.jooby.hibernate.extension.translatable.repository;

import io.xammax.jooby.hibernate.extension.translatable.model.Translation;
import io.xammax.jooby.hibernate.model.Persistable;
import io.xammax.jooby.hibernate.repository.EntityRepository;

import javax.annotation.Nullable;
import java.io.Serializable;
import java.util.List;

public interface TranslationRepository<E extends Persistable<ID>, ID extends Serializable>
    extends EntityRepository<E, ID> {

  TranslationRepository<E, ID> translate(E entity, String field, String language, String value);

  List<Translation> findTranslations(E entity);

  List<Translation> findTranslationsByObjectId(ID id);

  @Nullable E findObjectByTranslatedField(String field, String value, String cls);

}
