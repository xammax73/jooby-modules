package io.xammax.jooby.hibernate.extension.translatable.model;

import io.xammax.jooby.hibernate.model.AbstractPersistable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class AbstractPersonalTranslation extends AbstractPersistable<Long> {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false, unique = true, updatable = false)
  protected Long id;

  @Column(name = "language", length = 2)
  protected String language;

  @Column(name = "object")
  protected Object object;

  @Column(name = "field", length = 64)
  protected String field;

  @Column(name = "content")
  protected String content;

  @Override
  public Long getId() {
    return id;
  }

  public String getLanguage() {
    return language;
  }

  public AbstractPersonalTranslation setLanguage(String language) {
    this.language = language;
    return this;
  }

  public Object getObject() {
    return object;
  }

  public AbstractPersonalTranslation setObject(Object object) {
    this.object = object;
    return this;
  }

  public String getField() {
    return field;
  }

  public AbstractPersonalTranslation setField(String field) {
    this.field = field;
    return this;
  }

  public String getContent() {
    return content;
  }

  public AbstractPersonalTranslation setContent(String content) {
    this.content = content;
    return this;
  }

}
