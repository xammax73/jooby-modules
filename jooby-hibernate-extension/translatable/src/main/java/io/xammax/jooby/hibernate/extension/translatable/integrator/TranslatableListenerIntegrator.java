package io.xammax.jooby.hibernate.extension.translatable.integrator;

import io.xammax.jooby.hibernate.extension.translatable.listener.TranslatableListener;
import org.hibernate.boot.Metadata;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.event.service.spi.EventListenerRegistry;
import org.hibernate.event.spi.EventType;
import org.hibernate.integrator.spi.Integrator;
import org.hibernate.service.spi.SessionFactoryServiceRegistry;

import javax.annotation.Nonnull;

public class TranslatableListenerIntegrator implements Integrator {

  public static final TranslatableListenerIntegrator INSTANCE = new TranslatableListenerIntegrator();

  @Override
  public void integrate(
      Metadata metadata,
      SessionFactoryImplementor sessionFactory,
      @Nonnull SessionFactoryServiceRegistry serviceRegistry) {

    final EventListenerRegistry eventListenerRegistry =
        serviceRegistry.getService(EventListenerRegistry.class);

    eventListenerRegistry.appendListeners(EventType.POST_INSERT, TranslatableListener.INSTANCE);
    eventListenerRegistry.appendListeners(EventType.POST_LOAD, TranslatableListener.INSTANCE);
    eventListenerRegistry.appendListeners(EventType.POST_UPDATE, TranslatableListener.INSTANCE);
  }

  @Override
  public void disintegrate(
      SessionFactoryImplementor sessionFactory,
      SessionFactoryServiceRegistry serviceRegistry) {
    // do nothing
  }

}
