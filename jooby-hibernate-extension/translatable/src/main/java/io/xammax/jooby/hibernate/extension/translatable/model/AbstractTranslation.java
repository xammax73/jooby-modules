package io.xammax.jooby.hibernate.extension.translatable.model;

import io.xammax.jooby.hibernate.model.AbstractPersistable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class AbstractTranslation extends AbstractPersistable<Long> {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false, unique = true, updatable = false)
  protected Long id;

  @Column(name = "language", length = 2)
  protected String language;

  @Column(name = "object_class")
  protected String objectClass;

  @Column(name = "field", length = 64)
  protected String field;

  @Column(name = "foreign_key", length = 64)
  protected String foreignKey;

  @Column(name = "content")
  protected String content;

  @Override
  public Long getId() {
    return id;
  }

  public String getLanguage() {
    return language;
  }

  public AbstractTranslation setLanguage(String language) {
    this.language = language;
    return this;
  }

  public String getObjectClass() {
    return objectClass;
  }

  public AbstractTranslation setObjectClass(String objectClass) {
    this.objectClass = objectClass;
    return this;
  }

  public String getField() {
    return field;
  }

  public AbstractTranslation setField(String field) {
    this.field = field;
    return this;
  }

  public String getForeignKey() {
    return foreignKey;
  }

  public AbstractTranslation setForeignKey(String foreignKey) {
    this.foreignKey = foreignKey;
    return this;
  }

  public String getContent() {
    return content;
  }

  public AbstractTranslation setContent(String content) {
    this.content = content;
    return this;
  }

}
