package io.xammax.jooby.hibernate.extension.translatable.listener;

import org.hibernate.event.spi.*;
import org.hibernate.metadata.ClassMetadata;
import org.hibernate.persister.entity.EntityPersister;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
//import java.util.Locale;

public class TranslatableListener implements
    PostInsertEventListener,
    PostLoadEventListener,
    PostUpdateEventListener {

  public static final TranslatableListener INSTANCE = new TranslatableListener();

  /**
   * Language that is set on this listener. If entity being translated has language defined it
   * will override this one.
   */
  protected String language = "en";

  /* *
   * Locale that created by language that is set on this listener. If entity being translated has
   * locale defined it will override this one.
   */
  //protected Locale locale = new Locale(language);

  /**
   * Default language, this changes behavior to not update the original record field if language
   * that is used for updating is not default. This will load the default translation in other
   * languages if record is not translated yet.
   */
  private final String defaultLanguage = "en";

  /* *
   * Default locale created by default language, this changes behavior to not update the original
   * record field if locale that is used for updating is not default. This will load the default
   * translation in other locales if record is not translated yet.
   */
  //private final Locale defaultLocale = new Locale(defaultLanguage);

  /**
   * If this is set to false, when if entity does not have a translation for requested language
   * or locale it will show a blank value.
   */
  private final boolean translationFallback = false;

  /**
   * List of translations which do not have the foreign key generated yet. These translations
   * will be updated with new keys on postPersist event.
   */
  private final List<String> pendingTranslationInserts = new ArrayList<>();

  /**
   * Default translation value upon missing translation.
   */
  private @Nullable String defaultTranslationValue;

  /**
   * Checks for inserted object to create their translation foreign keys
   */
  public void onPostInsert(@Nonnull PostInsertEvent event) {

    final Object entity = event.getEntity();
    final ClassMetadata metadata = event.getPersister().getClassMetadata();

  }

  public void onPostLoad(@Nonnull PostLoadEvent event) {

    final Object entity = event.getEntity();
    final ClassMetadata metadata = event.getPersister().getClassMetadata();

  }

  /**
   * Checks for inserted object to update their translation foreign keys
   */
  public void onPostUpdate(@Nonnull PostUpdateEvent event) {

    final Object entity = event.getEntity();
    final ClassMetadata metadata = event.getPersister().getClassMetadata();

  }

  @Deprecated
  @Override
  public boolean requiresPostCommitHanding(EntityPersister persister) {
    return false;
  }

}
