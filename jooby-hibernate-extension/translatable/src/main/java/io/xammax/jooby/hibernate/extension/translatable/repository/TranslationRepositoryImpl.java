package io.xammax.jooby.hibernate.extension.translatable.repository;

import io.jooby.hibernate.SessionProvider;
import io.xammax.jooby.hibernate.extension.translatable.listener.TranslatableListener;
import io.xammax.jooby.hibernate.extension.translatable.model.Translation;
import io.xammax.jooby.hibernate.model.AbstractPersistable;
import io.xammax.jooby.hibernate.repository.EntityRepositoryImpl;
import org.hibernate.Metamodel;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.metadata.ClassMetadata;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static io.xammax.jooby.JoobyInstance.app;

public class TranslationRepositoryImpl<E extends AbstractPersistable<ID>, ID extends Serializable>
    extends EntityRepositoryImpl<E, ID> implements TranslationRepository<E, ID> {

  private final TranslatableListener listener;

  public TranslationRepositoryImpl(TranslatableListener listener) {
    super();
    this.listener = listener;
  }

  public TranslationRepository<E, ID> translate(@Nonnull E entity, String field, String language, String value) {
    SessionFactory sessionFactory = app.require(sessionFactoryKey);
    SessionProvider sessionProvider = app.require(sessionProviderKey);
    try(Session session = sessionProvider.newSession(sessionFactory.withOptions())) {
      Class<?> type = getMetadataUtils().getUnproxiedClass(entity.getClass());
      ClassMetadata metadata = sessionFactory.getClassMetadata(type);
      Metamodel metamodel = sessionFactory.getMetamodel();


    }

    return this;
  }

  public List<Translation> findTranslations(E entity) {
    List<Translation> result = new ArrayList<>();

    return result;
  }

  public List<Translation> findTranslationsByObjectId(ID id) {

  }

  public @Nullable E findObjectByTranslatedField(String field, String value, String objectClass) {

  }

  private TranslatableListener getTranslatableListener() {
    return this.listener;
  }

}
