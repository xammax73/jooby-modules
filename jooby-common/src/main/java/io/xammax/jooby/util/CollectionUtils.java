package io.xammax.jooby.util;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.Map;

public class CollectionUtils {

  /**
   * Returns {@code true} if the supplied {@link Collection} is {@code null} or empty, otherwise
   * returns {@code false}.
   *
   * @param collection the {@link Collection} to check
   * @return whether the given {@link Collection} is empty or {@literal null}
   */
  public static boolean isEmpty(@Nullable Collection<?> collection) {
    return collection == null || collection.isEmpty();
  }

  /**
   * Returns {@code true} if the supplied {@link Map} is {@code null} or empty, otherwise returns
   * {@code false}.
   *
   * @param map the {@link Map} to check
   * @return true whether the given {@link Map} is empty or {@literal null}
   */
  public static boolean isEmpty(@Nullable Map<?, ?> map) {
    return map == null || map.isEmpty();
  }


  /**
   * Returns {@code true} if the supplied {@link Collection} is not {@code null} and not empty,
   * otherwise returns {@code false}.
   *
   * @param collection the {@link Collection} to check
   * @return whether the given {@link Collection} is not empty and not {@literal null}
   */
  public static boolean isNotEmpty(@Nullable Collection<?> collection) {
    return collection != null && !collection.isEmpty();
  }

  /**
   * Returns {@code true} if the supplied {@link Map} is not {@code null} and not empty, otherwise
   * returns {@code false}.
   *
   * @param map the {@link Map} to check
   * @return true whether the given {@link Map} is not empty and not {@literal null}
   */
  public static boolean isNotEmpty(@Nullable Map<?, ?> map) {
    return map != null && !map.isEmpty();
  }

}
