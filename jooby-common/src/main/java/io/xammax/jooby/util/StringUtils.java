package io.xammax.jooby.util;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public final class StringUtils {

  private static final String[] EMPTY_STRING_ARRAY = {};

  private static final String FOLDER_SEPARATOR = "/";

  private static final String WINDOWS_FOLDER_SEPARATOR = "\\";

  private static final String TOP_PATH = "..";

  private static final String CURRENT_PATH = ".";

  private static final char EXTENSION_SEPARATOR = '.';

  /**
   * Check whether the given object (may be a {@link String}) is empty. This is effectively
   * a shortcut for {@code !hasLength(String)}. This method accepts any Object as an argument,
   * comparing it to {@literal null} and the empty String. As a consequence, this method will
   * never return {@literal true} for a non-null or non-String object. The Object signature is
   * useful for general attribute handling code that commonly deals with Strings but generally
   * has to iterate over Objects since attributes may e.g. be primitive value objects as well.
   *
   * Note: If the object is typed to {@literal String} upfront, prefer {@code #hasLength(String)}
   * or {@code #hasText(String)} instead.
   *
   * @param str the candidate object (possibly a {@link String})
   */
  public static boolean isEmpty(@Nullable Object str) {
    return (str == null || "".equals(str));
  }

  /**
   * Check that the given {@link CharSequence} is neither {@literal null} nor of length 0.
   *
   * Note: this method returns {@literal true} for a {@link CharSequence} that purely consists
   * of whitespace.
   *
   * <p><pre class="code">
   * StringUtils.hasLength(null) = false
   * StringUtils.hasLength("") = false
   * StringUtils.hasLength(" ") = true
   * StringUtils.hasLength("Hello") = true
   * </pre>
   *
   * @param str the {@link CharSequence} to check (may be {@literal null})
   * @return {@literal true} if the {@link CharSequence} is not {@literal null} and has length
   */
  public static boolean hasLength(@Nullable CharSequence str) {
    return (str != null && str.length() > 0);
  }

  /**
   * Check that the given {@link String} is neither {@literal null} nor of length 0.
   *
   * Note: this method returns {@literal true} for a {@link String} that purely consists of
   * whitespace.
   *
   * @param str the {@link String} to check (may be {@literal null})
   * @return {@literal true} if the {@link String} is not {@literal null} and has length
   */
  public static boolean hasLength(String str) {
    return (str != null && !str.isEmpty());
  }

  /**
   * Check whether the given {@link CharSequence} contains actual text. More specifically,
   * this method returns {@literal true} if the {@link CharSequence} is not {@literal null},
   * its length is greater than 0, and it contains at least one non-whitespace character.
   *
   * <p><pre class="code">
   *     StringUtils.hasText(null) = false
   *     StringUtils.hasText("") = false
   *     StringUtils.hasText(" ") = false
   *     StringUtils.hasText("12345") = true
   *     StringUtils.hasText(" 12345 ") = true
   * </pre>
   *
   * @param str the {@link CharSequence} to check (may be {@literal null})
   * @return {@literal true} if the {@link CharSequence} is not {@literal null}, its length
   *         is greater than 0, and it does not contain whitespace only
   */
  public static boolean hasText(@Nullable CharSequence str) {
    return (str != null && str.length() > 0 && containsText(str));
  }

  /**
   * Check whether the given {@link String} contains actual text. More specifically, this
   * method returns {@literal true} if the {@link String} is not {@literal null} its length
   * is greater than 0, and contains at least one non-whitespace character.
   *
   * @param str the {@literal String} to check (may be {@literal null})
   * @return {@literal true} if the {@literal String} is not {@literal null}, its length
   *         is greater than 0, and it does not contain whitespace only
   */
  public static boolean hasText(@Nullable String str) {
    return (str != null && !str.isEmpty() && containsText(str));
  }







  private static boolean containsText(@Nonnull CharSequence str) {
    int strLen = str.length();

    for (int i = 0; i < strLen; i++) {
      if (!Character.isWhitespace(str.charAt(i))) {
        return true;
      }
    }

    return false;
  }

}
