package io.xammax.jooby.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EventManagerImpl<E extends Event> implements EventManager<E> {

  private static final Logger log = LoggerFactory.getLogger(EventManagerImpl.class);
  //private static final String PROPERTY_PREFIX = EventManagerImpl.class.getName();

  /**
   * This {@link HashMap} contains all listeners inherited from {@link EventListener} type
   * with all registered event types inherited from {@link Event}.
   */
  private Map<String, List<EventListener<E>>> mapListeners;

  public EventManagerImpl() {
    this.mapListeners = new HashMap<>();
  }

  public Map<String, List<EventListener<E>>> getMapListeners() {
    return mapListeners;
  }

  /* *public EventManagerImpl(String... eventTypes) {
  //  for (String eventType : eventTypes) {
  //    this.mapListeners.put(eventType, new ArrayList<>());
  //  }
  }*/

  //@SuppressWarnings("unchecked")
  public void publish(@Nonnull E event) {
    String eventType = event.getEventType();
    List<EventListener<E>> appListeners = getMapListeners().get(eventType);
    if (getMapListeners().get(eventType).size() != 0) {
      for (EventListener<E> listener : appListeners) {
        listener.handleEvent(event);
      }
    }
  }

  public void register(@Nonnull String eventType, @Nonnull EventListener<E> listener, boolean useCapture) {
    List<EventListener<E>> appListeners;

    if (!getMapListeners().isEmpty() && getMapListeners().get(eventType).size() != 0) {
      appListeners = getMapListeners().get(eventType);
      if (!appListeners.contains(listener)) {
        appListeners.add(listener);
        getMapListeners().remove(eventType);
        getMapListeners().put(eventType, appListeners);
      }
    } else {
      appListeners = new ArrayList<>();
      appListeners.add(listener);
      getMapListeners().put(eventType, appListeners);
    }
  }

  public void unregister(@Nonnull String eventType, @Nonnull EventListener<E> listener, boolean useCapture) {
    List<EventListener<E>> appListeners;

    if (!getMapListeners().isEmpty() && getMapListeners().get(eventType).size() != 0) {
      appListeners = getMapListeners().get(eventType);

      if (appListeners.remove(listener)) {
        getMapListeners().remove(eventType);
        getMapListeners().put(eventType, appListeners);
      }
    }
  }

}
