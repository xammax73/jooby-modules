package io.xammax.jooby.event;

import javax.annotation.Nonnull;
import java.util.EventObject;

public abstract class Event extends EventObject {

  /** Uses serialVersionUID for interoperability. */
  private static final long serialVersionUID = 7099057708183571937L;

  /** Event type when the event happened. */
  private final String eventType;

  /** System time when the event happened. */
  private final long timestamp;

  /**
   * Creates a new {@code Event}.
   *
   * @param source the object on which the event initially occurred or with which the event
   *               is associated (never {@code null})
   * @param eventType specifies event type that must not be {@code null}
   */
  public Event(@Nonnull Object source, String eventType) {
    super(source);
    this.eventType = eventType;
    this.timestamp = System.currentTimeMillis();
  }

  /**
   * Returns the event type.
   */
  public String getEventType() {
    return eventType;
  }

  /**
   * Returns the system time in milliseconds when the event occurred.
   */
  public final long getTimestamp() {
    return this.timestamp;
  }

}
